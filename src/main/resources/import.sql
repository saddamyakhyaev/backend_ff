INSERT INTO cliptype (icon, name) VALUES ('student.png', 'Группа'), ('teacher.png', 'Преподаватель'), ('door.png', 'Кабинет');
INSERT INTO users (createdDate, email, name, password, state) VALUES ('2021-12-19 22:19:51','saddam.yakhyaev@gmail.com','admin','$2a$10$VSWtMGhJ5eaCbCdRXfsPQe3j9Jf0zaReaGDNvCVdnhX4FP0wik1IK',0),('2021-12-19 22:19:51','bot.saddam.yakhyaev@gmail.com','bot','$2a$10$VSWtMGhJ5eaCbCdRXfsPQe3j9Jf0zaReaGDNvCVdnhX4FP0wik1IK',0);

INSERT INTO userbox (lastNews, user_id, mainClip_id) VALUES (0, 1, null);
INSERT INTO userbox (lastNews, user_id, mainClip_id) VALUES (0, 2, null);

INSERT INTO user_role (user_id, roles) VALUES (1, 0);
INSERT INTO user_role (user_id, roles) VALUES (2, 1);