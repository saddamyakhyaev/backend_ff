package ru.findoffind.ff.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.findoffind.ff.dao.ClipDao;
import ru.findoffind.ff.dao.DonatDao;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.UserBox;
import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.donation.Donat;
import ru.findoffind.ff.entity.event.EventLimit;
import ru.findoffind.ff.payload.request.DonatCreateRequest;
import ru.findoffind.ff.payload.request.EventLimitCreateRequest;

import java.security.Principal;
import java.util.List;

@Service
public class DonatService {

     @Autowired
     private DonatDao donatDao;

    @Autowired
    private UserService userService;

    @Autowired
    private ClipDao clipDao;

    /**
     * Метод создаёт или дополняет донат
     * @param createRequest данные для создания
     */
    @Transactional
    public boolean createOrAddDonat(DonatCreateRequest createRequest, Principal principal){

        UserBox userBox = userService.getUserBoxById(createRequest.getUserId());
        Donat donat = donatDao.getDonatById(createRequest.getClipId());

        if(donat == null){
            Clip clip = clipDao.getClip(createRequest.getClipId());
            if(clip == null) return false;

            donat = new Donat();
            donat.setCoins(0);
            donat.setClip(clip);
        }

        donat.setCoins(donat.getCoins() + createRequest.getCoins());
        userBox.setCoins(userBox.getCoins() + createRequest.getCoins());

        return donatDao.saveDonat(donat);
    }

    /**
     * Получить список всех донатов
     */
    @Transactional
    public List<Donat> getAllDonats() {
        return donatDao.getAllDonats();
    }
}
