package ru.findoffind.ff.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.findoffind.ff.dao.MarkDao;
import ru.findoffind.ff.dao.NewsDao;
import ru.findoffind.ff.dao.UserDao;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.UserBox;
import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.clip.ClipType;
import ru.findoffind.ff.entity.enums.EState;
import ru.findoffind.ff.entity.event.EventSeason;
import ru.findoffind.ff.entity.mark.Mark;
import ru.findoffind.ff.entity.news.News;
import ru.findoffind.ff.entity.news.NewsBody;
import ru.findoffind.ff.payload.request.ClipCreateRequest;
import ru.findoffind.ff.payload.request.NewsCreateRequest;

import java.security.Principal;
import java.util.List;

/**
 * Сервис для работы новостей {@link News}
 */
@Service
public class NewsService {

    @Autowired
    private NewsDao newsDao;

    @Autowired
    private MarkDao markDao;

    @Autowired
    private UserService userService;

    /**
     * Метод создаёт новую новость
     * @param createRequest данные для создания новости
     * @return результат
     */
    @Transactional
    public boolean createNews(NewsCreateRequest createRequest, Principal principal){

        User authUser = userService.getUserByPrincipal(principal);
        if(authUser == null) return false;

        News news;
        NewsBody newsBody;

        if(createRequest.getId() != null){
            news = newsDao.getNews(createRequest.getId());
            newsBody = newsDao.getNewsBody(createRequest.getId());
            if(news == null || newsBody == null) return false;

            news.getMarks().clear();
        }else{
            news = new News();
            newsBody = new NewsBody();

            news.setIsActive(true);
            news.setIsPinned(false);
        }

        news.setName(createRequest.getName());
        news.setDescription(createRequest.getDescription());
        news.setDateFrom(createRequest.getDateFrom());
        news.setDateTo(createRequest.getDateTo());

        newsBody.setText(createRequest.getText());

        if(createRequest.getNameUnique() != null)
            news.setNameUnique(createRequest.getNameUnique());

        if(createRequest.getIsNotification() != null)
            news.setIsNotification(createRequest.getIsNotification());
        else news.setIsNotification(false);


        for(Long markId: createRequest.getMarksId()){
            Mark mark = markDao.getMarkById(markId);
            if(mark == null) return false;

            news.getMarks().add(mark);
        }

        news.setCreateUser(authUser);

        newsDao.saveNews(news);

        if(createRequest.getId() == null) newsBody.setId(news.getId());
        newsDao.saveNewsBody(newsBody);

        return true;
    }

    /**
     * Получить тело новости
     * @param newsId if новости
     * @return результат
     */
    @Transactional
    public NewsBody getNewsBody(Long newsId) {
        return newsDao.getNewsBody(newsId);
    }

    /**
     * Получаем все активные новости
     * @return сезоны событий
     */
    @Transactional
    public List<News> getAllNews() {
        return newsDao.getAllActiveNews();
    }

    /**
     * Закрепляем или открепляем новость
     * @param newsId новости
     */
    @Transactional
    public Boolean isPinnedOrNotNews(boolean flag, Long newsId, Principal principal) {

        News news = newsDao.getNews(newsId);
        news.setIsPinned(flag);
        return true;
    }

    /**
     * Активизируем или наоборот новость
     * @param newsId новости
     */
    @Transactional
    public Boolean isActiveOrNotNews(boolean flag, Long newsId, Principal principal) {

        News news = newsDao.getNews(newsId);
        news.setIsActive(flag);
        return true;
    }

    /**
     * Сохраняем последнюю новость, что видел пользователь
     * @param newsId id новости
     * @return результат
     */
    @Transactional
    public boolean setUserLastNews(Long newsId, Principal principal) {
        User authUser = userService.getUserByPrincipal(principal);
        if(authUser == null) return false;

        UserBox userBox = userService.getUserBoxById(authUser.getId());
        if(userBox == null) return false;

        userBox.setLastNews(newsId);

        return true;
    }
}
