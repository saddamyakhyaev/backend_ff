package ru.findoffind.ff.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.findoffind.ff.dao.ClipDao;
import ru.findoffind.ff.dao.EventDao;
import ru.findoffind.ff.dao.WarnDao;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.enums.EType;
import ru.findoffind.ff.entity.event.Event;
import ru.findoffind.ff.entity.warn.*;
import ru.findoffind.ff.payload.request.NewsCreateRequest;
import ru.findoffind.ff.payload.request.WarnCreateRequest;

import java.security.Principal;
import java.util.List;

@Service
public class WarnService {

    @Autowired
    private UserService userService;

    @Autowired
    private WarnDao warnDao;

    @Autowired
    private ClipDao clipDao;

    @Autowired
    private EventDao eventDao;

    /**
     * Метод создаёт новое предупреждение
     */
    @Transactional
    public boolean sendWarn(WarnCreateRequest createRequest, Principal principal){
        User authUser = userService.getUserByPrincipal(principal);
        if(authUser == null) return false;

        Warn warn = new Warn();
        warn.setText(createRequest.getText());
        warn.setCodeWarn(createRequest.getCodeWarn());
        warn.setCreateUserId(authUser.getId());
        if(createRequest.getObjectLink() != null) warn.setObjectLink(createRequest.getObjectLink());

        if(createRequest.getObjectType() != null && createRequest.getObjectType() != -1){
            if(createRequest.getObjectId() != null) {
                if (createRequest.getObjectType() == EType.TYPE_CLIP.ordinal()) {
                    Clip clip = clipDao.getClip(createRequest.getObjectId());
                    if (clip != null) warn.setUserId(clip.getCreateUserId());
                } else if (createRequest.getObjectType() == EType.TYPE_EVENT.ordinal()) {
                    Event event = eventDao.getEventById(createRequest.getObjectId());
                    if (event != null) warn.setUserId(event.getCreateUserId());
                }
                warn.setObjectType(createRequest.getObjectType());
                warn.setObjectId(createRequest.getObjectId());
            }
        }

        return warnDao.saveWarn(warn);
    }

    /**
     * Получаем последние предупреждения
     */
    @Transactional
    public List<Warn> getLastWarns() {
        return warnDao.getLastWarns(true);
    }

    /**
     * Очищаем все предупреждения
     */
    @Transactional
    public boolean clearAllWarn() {
        List<Warn> warns = warnDao.getLastWarns(false);
        for(Warn warn: warns) {
            warnDao.deleteWarn(warn);
        }

        return true;
    }

    /**
     * Получаем последние предупреждения по пользователю
     */
    @Transactional
    public List<Warn> getLastWarnsByUser(Long userId) {
        return warnDao.getLastWarnsByUser(userId);
    }

    /**
     * Очищаем предупреждения по пользователю
     */
    @Transactional
    public boolean clearLastWarnsByUser(Long userId) {
        List<Warn> warns = warnDao.getLastWarnsByUser(userId);
        for(Warn warn: warns) {
            warnDao.deleteWarn(warn);
        }

        return true;
    }



}
