package ru.findoffind.ff.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.findoffind.ff.dao.UserDao;
import ru.findoffind.ff.dto.FindDto;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.enums.ERole;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Service
public class FindService {


    @Autowired
    private UserService userService;

    @Autowired
    private ClipService clipService;

    @Transactional
    public FindDto find(String str, Principal principal) {

        FindDto find = new FindDto();
        User currentUser = userService.getUserByPrincipal(principal);


        find.setClips(clipService.getClipsToFind(str));

        find.setUsers(new ArrayList<>());
        if (currentUser != null
                && (currentUser.isUserRole(ERole.ROLE_MODERATOR_3)
                || currentUser.isUserRole(ERole.ROLE_ADMIN))){

            find.setUsers(userService.getUsersToFind(str, currentUser));
        }


        return find;
    }



}
