package ru.findoffind.ff.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.findoffind.ff.dao.ClipDao;
import ru.findoffind.ff.dto.ClipDto;
import ru.findoffind.ff.dto.UserDto;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.UserBox;
import ru.findoffind.ff.entity.clip.*;
import ru.findoffind.ff.entity.enums.ERole;
import ru.findoffind.ff.entity.enums.EState;
import ru.findoffind.ff.entity.event.Event;
import ru.findoffind.ff.facade.ClipFacade;
import ru.findoffind.ff.payload.request.ClipCreateRequest;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Сервис для работы {@link Clip}
 * а так же:
 * - {@link ClipType}
 */
@Service
public class ClipService {

    @Autowired
    private UserService userService;

    @Autowired
    private ClipDao clipDao;

    @Autowired
    private ClipFacade clipFacade;

    /**
     * Получаем все типы закрепов
     * @return типы закрепов
     */
    @Transactional
    public List<ClipType> getClipTypes() {
        return clipDao.getClipTypes();
    }

    /**
     * Метод создаёт новый закреп
     * @param createRequest данные для создания закрепа
     * @return результат
     */
    @Transactional
    public boolean createClip(ClipCreateRequest createRequest, Principal principal){

        User authUser = userService.getUserByPrincipal(principal);
        if(authUser == null) return false;

        if(clipDao.getTypeClip(createRequest.getClipTypeId()) == null) return false;

        Clip createClip = null;

        if(createRequest.getId() == null){

            if(clipDao.getClipOfName(createRequest.getName()) != null) return false;
            createClip = new Clip();
            createClip.setCountFavoriteUser(0);
            createClip.setCountMainUser(0);
            createClip.setCreateUserId(authUser.getId());
        }else {
            createClip = clipDao.getClip(createRequest.getId());

            if(!authUser.isUserRole(ERole.ROLE_MODERATOR_3) && !authUser.isUserRole(ERole.ROLE_ADMIN)){
                if(createClip.getCreateUserId() != authUser.getId()) return false;
            }

        }

        createClip.setName(createRequest.getName());
        createClip.setDescription(createRequest.getDescription());
        createClip.setTypeClipId(createRequest.getClipTypeId());
        createClip.setState(EState.STATE_NORMAL.ordinal());
        createClip.setMarkId(createRequest.getMarkId());

        if(createRequest.getInformation() != null) createClip.setInformation(createRequest.getInformation());

        return clipDao.saveClip(createClip);
    }



    /**
     * Возвращает нужный закреп
     * @param clipId id закрепа
     * @return искомый закреп
     */
    @Transactional
    public Clip getClipById(Long clipId) {
        return clipDao.getClip(clipId);
    }

    /**
     * Возвращаем все закрепы
     */
    @Transactional
    public List<Clip> getAllClips() {
        return clipDao.getAllClips();
    }

    /**
     * Возвращаем все закрепы созданные определенным пользователем
     * @param userId id пользователя
     * @return искомые закрепы
     */
    @Transactional
    public List<Clip> getClipsCreatingByUser(Long userId) {
        return clipDao.getClipsCreatingByUser(userId);
    }

    /**
     * Добавляем или удаляем закреп в список избранных
     * @param clipId закреп
     */
    @Transactional
    public Boolean addOrDeleteInFavouritesClip(boolean flag, Long clipId, Principal principal) {

        User authUser = userService.getUserByPrincipal(principal);
        if(authUser == null) return false;

        UserBox userBox = userService.getUserBoxById(authUser.getId());
        if(userBox == null) return false;


        Clip clip = clipDao.getClip(clipId);
        if(clip == null) return false;

        if(flag){
            if(userBox.getFavouritesClips().size() > 500) return false;
            userBox.getFavouritesClips().add(clip);
            clip.setCountFavoriteUser(clip.getCountFavoriteUser() + 1);
        } else {
            List<Clip> favouritesClips = userBox.getFavouritesClips();
            for(int i = 0; i < favouritesClips.size(); i++){
                if(favouritesClips.get(i).getId() == clipId) {
                    favouritesClips.remove(i);
                    clip.setCountFavoriteUser(clip.getCountFavoriteUser() - 1);
                    break;
                }
            }

            if(userBox.getMainClip() != null)
                if(userBox.getMainClip().getId() == clipId) {
                    clip.setCountMainUser(clip.getCountMainUser() - 1);
                    userBox.setMainClip(null);
                }
        }

        return true;
    }

    /**
     * Добавляем главный закреп
     * @param clipId закреп
     */
    @Transactional
    public Boolean addMainClip(Long clipId, Principal principal) {

        User authUser = userService.getUserByPrincipal(principal);
        if(authUser == null) return false;

        UserBox userBox = userService.getUserBoxById(authUser.getId());
        if(userBox == null) return false;

        Clip clip = clipDao.getClip(clipId);
        if(clip == null) return false;

        userBox.setMainClip(clip);
        clip.setCountMainUser(clip.getCountMainUser() + 1);

        return true;
    }




    /**
     * Метод для поиска закрепов,
     * Главным образом вызывается из {@link FindService}
     * @param str - строка поиска
     * @return Список соответсвующих закрепов
     */
    @Transactional
    public List<ClipDto> getClipsToFind(String str) {

        List<Clip> clips = clipDao.getClipsToFindToName(str);
        return clips.stream().map(clipFacade::getClipDto).collect(Collectors.toList());
    }




//    @Autowired
//    private ClipD clipDto;
//
//    public List<UserDto> getClipsToFind(String str) {
//        List<User> users = userDao.getAllUsers();
//        return users.stream().map(userFacade::getUserDto).collect(Collectors.toList());
//    }
}
