package ru.findoffind.ff.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.findoffind.ff.dao.MarkDao;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.event.EventLimit;
import ru.findoffind.ff.entity.mark.Mark;
import ru.findoffind.ff.entity.news.News;
import ru.findoffind.ff.payload.request.EventLimitCreateRequest;
import ru.findoffind.ff.payload.request.MarkCreateRequest;

import java.security.Principal;
import java.util.List;

/**
 * Сервис для работы маркировки {@link Mark}
 */
@Service
public class MarkService {


    @Autowired
    private MarkDao markDao;

    @Autowired
    private UserService userService;

    /**
     * Получаем все марки
     */
    @Transactional
    public List<Mark> getAllMark() {

        List<Mark> marks = markDao.getAllMark();
        return marks;
    }

    /**
     * Метод создаёт новую марку
     */
    @Transactional
    public boolean createMark(MarkCreateRequest createRequest, Principal principal){

        User authUser = userService.getUserByPrincipal(principal);
        if(authUser == null) return false;

        Mark mark = null;

        if(createRequest.getId() == null){
            mark = new Mark();
        }else{
            mark = markDao.getMarkById(createRequest.getId());
        }

        mark.setName(createRequest.getName());
        if(createRequest.getParentMarkId() != null)
            mark.setParentMarkId(createRequest.getParentMarkId());

        return markDao.saveMark(mark);
    }
}
