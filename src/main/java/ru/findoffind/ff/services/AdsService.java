package ru.findoffind.ff.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.findoffind.ff.dao.AdsDao;
import ru.findoffind.ff.dao.NewsDao;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.ads.Ads;
import ru.findoffind.ff.entity.ads.AdsBody;
import ru.findoffind.ff.entity.news.News;
import ru.findoffind.ff.entity.news.NewsBody;
import ru.findoffind.ff.payload.request.AdsCreateRequest;
import ru.findoffind.ff.payload.request.NewsCreateRequest;

import java.security.Principal;
import java.util.List;

/**
 * Сервис для работы новостей {@link Ads}
 */
@Service
public class AdsService {

    @Autowired
    private AdsDao adsDao;

    @Autowired
    private UserService userService;

    /**
     * Метод создаёт новую рекламу
     * @param createRequest данные для создания рекламы
     * @return результат
     */
    @Transactional
    public boolean createAds(AdsCreateRequest createRequest, Principal principal){

        User authUser = userService.getUserByPrincipal(principal);
        if(authUser == null) return false;

        Ads ads = new Ads();
        AdsBody adsBody = new AdsBody();

        ads.setName(createRequest.getName());
        ads.setDescription(createRequest.getDescription());
        ads.setDateFrom(createRequest.getDateFrom());
        ads.setDateTo(createRequest.getDateTo());
        ads.setImageUrlPc(createRequest.getImageUrlPc());
        ads.setImageUrlMobil(createRequest.getImageUrlMobil());

        if(createRequest.getLink() != null) ads.setLink(createRequest.getLink());
        else ads.setLink(null);

        ads.setIsActive(true);
        ads.setCreateUser(authUser);
        adsDao.saveNews(ads);

        if(createRequest.getText() != null){
            adsBody.setText(createRequest.getText());
            adsBody.setId(ads.getId());
            adsDao.saveAdsBody(adsBody);
        }

        return true;
    }


    /**
     * Получить тело рекламы
     * @param adsId id рекламы
     * @return результат
     */
    @Transactional
    public AdsBody getAdsBody(Long adsId) {
        return adsDao.getAdsBody(adsId);
    }

    /**
     * Получаем все активные рекламы
     * @return сезоны рекламы
     */
    @Transactional
    public List<Ads> getAllAds() {
        return adsDao.getAllActiveAds();
    }

    /**
     * Активизируем или наоборот рекламы
     * @param adsId новости
     */
    @Transactional
    public Boolean isActiveOrNotAds(boolean flag, Long adsId, Principal principal) {

        Ads ads = adsDao.getAds(adsId);
        ads.setIsActive(flag);
        return true;
    }

}
