package ru.findoffind.ff.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.findoffind.ff.dao.EventDao;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.clip.ClipType;
import ru.findoffind.ff.entity.enums.ERole;
import ru.findoffind.ff.entity.enums.EState;
import ru.findoffind.ff.entity.event.*;
import ru.findoffind.ff.payload.request.*;

import java.security.Principal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.List;

/**
 * Сервис для работы с событиями {@link Event}
 * и так же:
 * - {@link EventTime}
 * - {@link EventSeason}
 */
@Service
public class EventService {

    @Autowired
    private EventDao eventDao;

    @Autowired
    private UserService userService;

    @Autowired
    private ClipService clipService;

    /**
     * Метод создаёт новый сезон событий
     * @param createRequest данные для создания
     * @return результат
     */
    @Transactional
    public EventSeason createEventSeason(EventSeasonCreateRequest createRequest, Principal principal){

        User authUser = userService.getUserByPrincipal(principal);
        if(authUser == null) return null;

        EventSeason eventSeason;

        // Если есть id, то мы меняем сезон, в обратном случае создаём
        if(createRequest.getId() == null){
            eventSeason = new EventSeason();
        }else {
            eventSeason = this.eventDao.getEventSeasonById(createRequest.getId());
        }

        eventSeason.setName(createRequest.getName());
        eventSeason.setDescription(createRequest.getDescription());
        eventSeason.setDateFrom(createRequest.getDateFrom());
        eventSeason.setDateTo(createRequest.getDateTo());
        eventSeason.setState(EState.STATE_BLOCK.ordinal());

        return eventDao.saveEventSeason(eventSeason);
    }

    /**
     * Получаем все сезоны событий
     * @return сезоны событий
     */
    @Transactional
    public List<EventSeason> getAllEventSeasons() {

        List<EventSeason> eventSeasons = eventDao.getAllEventSeasons();
        eventSeasons.sort(EventSeason::compareTo);

        return eventSeasons;
    }

    /**
     * Удаляем сезон
     * @return результат
     */
    @Transactional
    public boolean deleteEventSeason(Long eventSeasonId) {

        EventSeason eventSeason = eventDao.getEventSeasonById(eventSeasonId);
        if(eventSeason == null) return false;

        for(List<Event> events = eventDao.getAllEventBySeason(eventSeason); events.size() > 0;) {
            for(Event event: events) {
                eventDao.deleteEvent(event);
            }

            events = eventDao.getAllEventBySeason(eventSeason); events.size();
        }

        return eventDao.deleteEventSeason(eventSeason);
    }

    /**
     * Активируем или блокируем сезон
     * @return результат
     */
    @Transactional
    public boolean activeOrBlockEventSeason(Long eventSeasonId, boolean flag) {

        EventSeason eventSeason = eventDao.getEventSeasonById(eventSeasonId);
        if(eventSeason == null) return false;

        if(flag) {
            eventSeason.setState(EState.STATE_NORMAL.ordinal());
        }else {
            eventSeason.setState(EState.STATE_BLOCK.ordinal());
        }

        return true;
    }


    /**
     * Метод создаёт новые границы событий
     * @param createRequest данные для создания
     * @return результат
     */
    @Transactional
    public boolean createEventLimit(EventLimitCreateRequest createRequest, Principal principal){

        User authUser = userService.getUserByPrincipal(principal);
        if(authUser == null) return false;

        EventLimit eventLimit = null;

        // Если есть id, то мы меняем лимит, в обратном случае создаём

        if(createRequest.getId() == null){
            eventLimit = new EventLimit();
        }else{
            eventLimit = this.eventDao.getEventLimitById(createRequest.getId());
        }

        eventLimit.setName(createRequest.getName());
        eventLimit.setTimeFrom(createRequest.getTimeFrom());
        eventLimit.setTimeTo(createRequest.getTimeTo());

        return eventDao.saveEventLimit(eventLimit);
    }

    /**
     * Получаем все границы событий
     * @return сезоны событий
     */
    @Transactional
    public List<EventLimit> getAllEventLimits() {

        List<EventLimit> eventLimits = eventDao.getAllEventLimits();
        eventLimits.sort(EventLimit::compareTo);

        return eventLimits;
    }

    /**
     * Удаляем лимит
     * @return результат
     */
    @Transactional
    public boolean deleteEventLimit(Long eventLimitId) {

        EventLimit eventLimit = eventDao.getEventLimitById(eventLimitId);
        if(eventLimit == null) return false;

        return eventDao.deleteEventLimit(eventLimit);
    }


    public EventTime getEventTime(EventTimeCreateRequest createRequest) {

        EventTime eventTime = new EventTime();
        eventTime.setTimeFrom(createRequest.getTimeFrom());
        eventTime.setTimeTo(createRequest.getTimeTo());
        eventTime.setDates(createRequest.getDates());

        this.eventDao.saveEventTime(eventTime);
        return eventTime;
    }


    /**
     * Метод создаёт новое событие
     * @param createRequest данные для создания
     * @return результат
     */
    @Transactional
    public boolean createEvent(EventCreateRequest createRequest, Principal principal){

        User authUser = userService.getUserByPrincipal(principal);
        if(authUser == null) return false;

        Event event = null;

        // Если есть id, то мы меняем событие, в обратном случае создаём
        if(createRequest.getId() == null){
            event = new Event();
            event.setCreateUserId(authUser.getId());
            event.setState(EState.STATE_NORMAL.ordinal());
        }else {
            event = this.eventDao.getEventById(createRequest.getId());
            event.getTimes().clear();
            event.getClips().clear();
        }

        event.setDescription(createRequest.getDescription());
        // Добавляем закрепы к событию
        for(Long clipId: createRequest.getClipsId()){
            Clip clip = clipService.getClipById(clipId);
            if(clip == null) return false;
            else event.addClip(clip);
        }
        // Добавляем сезон к событию
        EventSeason eventSeason = eventDao.getEventSeasonById(createRequest.getEventSeasonId());
        if(eventSeason != null) event.setSeason(eventSeason);
        // Добавляем время к событию
        for(EventTimeCreateRequest eventTime: createRequest.getTimes()){
            event.getTimes().add(getEventTime(eventTime));
        }

        if(createRequest.getInformation() != null) event.setInformation(createRequest.getInformation());

        return eventDao.saveEvent(event);
    }

    /**
     * Удаляем событие
     * @return результат
     */
    @Transactional
    public boolean deleteEvent(Long eventId, Principal principal) {

        User authUser = userService.getUserByPrincipal(principal);
        Event event = eventDao.getEventById(eventId);

        if(event == null) return false;
        if(!authUser.isUserRole(ERole.ROLE_MODERATOR_3) && !authUser.isUserRole(ERole.ROLE_ADMIN)){
            if(!event.getCreateUserId().equals(authUser.getId())) return false;
        }

        event.setState(EState.STATE_DELETE.ordinal());
        return true;
    }

    /**
     * Получаем все события закрепа
     */
    @Transactional
    public List<Event> getAllEventByClip(Long clipId) {

        List<Event> eventLimits = eventDao.getAllEventByClip(clipId);
        return eventLimits;
    }

    /**
     * Получаем все события по закрепу определенного сезона
     */
    @Transactional
    public List<Event> getAllEventByClipAndBySeason(Long clipId, Long seasonId) {

        List<Event> eventLimits = eventDao.getAllEventByClip(clipId);
        return eventLimits;
    }


}
