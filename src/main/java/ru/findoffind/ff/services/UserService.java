package ru.findoffind.ff.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.findoffind.ff.dao.ClipDao;
import ru.findoffind.ff.dao.EventDao;
import ru.findoffind.ff.dao.UserDao;
import ru.findoffind.ff.dto.UserDto;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.UserBox;
import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.enums.ERole;
import ru.findoffind.ff.entity.enums.EState;
import ru.findoffind.ff.entity.event.Event;
import ru.findoffind.ff.exceptions.UserExistException;
import ru.findoffind.ff.facade.UserFacade;
import ru.findoffind.ff.payload.request.SignupRequest;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    public static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private EventDao eventDao;

    @Autowired
    private UserFacade userFacade;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    /**
     * Регистрируем пользователя
     * @param userIn Данные для регистрации
     * @return возвращаем созданного пользователя
     */
    @Transactional
    public User createUser(SignupRequest userIn) {

        if(userDao.getUserOfEmail(userIn.getEmail()) != null) {
            return null;
        }

        User user = new User();
        user.setEmail(userIn.getEmail());
        user.setName(userIn.getName());
        user.setPassword(passwordEncoder.encode(userIn.getPassword()));
        user.getRoles().add(ERole.ROLE_USER);
        user.setState(EState.STATE_NORMAL.ordinal());

        UserBox userBox = new UserBox();
        userBox.setLastNews(0L);
        userBox.setCoins(0);
        userBox.setUser(user);

        userDao.saveUserBox(userBox);
        userDao.saveUser(user);

        try {
            LOG.info("Saving User {}", userIn.getEmail());
            if(userDao.saveUser(user)) return user;
            else return null;
        } catch (Exception e) {
            LOG.error("Error during registration. {}", e.getMessage());
            throw new UserExistException("The user " + user.getUsername() + " already exist. Please check credentials");
        }
    }

    /**
     * Возвращаем пользователя соотвествующего principal'у
     * @return возвращаем созданного пользователя
     */
    @Transactional
    public User getCurrentUser(Principal principal) {

        User user = getUserByPrincipal(principal);
        if(user == null) throw new UsernameNotFoundException("Username not found with username ");
        return user;
    }

    @Transactional
    public User getUserById(Long userId) {
        return userDao.getUser(userId);
    }

    @Transactional
    public UserBox getUserBoxById(Long userId) {
        return userDao.getUserBox(userId);
    }


    /**
     * Метод для поиска пользователей,
     * Главным образом вызывается из {@link FindService}
     * @param str - строка поиска
     * @param currentUser - авторизованный пользователей
     * @return Список соответсвующих пользователей
     */
    @Transactional
    public List<UserDto> getUsersToFind(String str, User currentUser) {

        List<User> users = userDao.getUsersToFindToName(str);
        if(str.matches("[-+]?\\d+")) users.addAll(userDao.getUsersToFindById(Long.valueOf(str)));
        return users.stream().map(userFacade::getUserDto).collect(Collectors.toList());
    }

    /**
     * Устанавливаем роль пользователю
     * @param userId - пользовательский ID
     * @return успешно/неуспешно
     */
    @Transactional
    public boolean setRoleUser(Long userId, ERole role, Principal principal){

        User authUser = getUserByPrincipal(principal);
        if(authUser == null) return false;

        User fUser = userDao.getUser(userId);
        if(fUser == null) return false;
        if(fUser.getProtectedId() != null)
            if(fUser.getProtectedId() != authUser.getId()) return false;


        switch (role) {

            case ROLE_ADMIN:
                break;

            case ROLE_MODERATOR_3:
                if(authUser.getRole() < ERole.ROLE_MODERATOR_3.ordinal()){
                    fUser.getRoles().add(ERole.ROLE_MODERATOR_3);
                    fUser.setProtectedId(authUser.getId());
                }
                break;
            case ROLE_MODERATOR_2:
                if(authUser.getRole() < ERole.ROLE_MODERATOR_2.ordinal()){
                    fUser.getRoles().add(ERole.ROLE_MODERATOR_2);
                    fUser.setProtectedId(authUser.getId());
                }
                break;
        }

        userDao.saveUser(fUser);

        return true;
    }


    /**
     * Делаем все действия для блокировки и разблокировки пользователя
     * @param userId id блокируемого пользователя
     * @return
     */
    @Transactional
    public boolean getBlockingUser(boolean flag, Long userId, Principal principal) {
        User authUser = getUserByPrincipal(principal);
        if(authUser == null) return false;

        User blockUser = userDao.getUser(userId);
        if(blockUser == null) return false;

        Integer state;
        if(flag) state = EState.STATE_BLOCK.ordinal();
        else state = EState.STATE_NORMAL.ordinal();

        blockUser.setState(state);

        List<Event> events = eventDao.getAllEventByUser(userId);
        events.forEach(event -> {

            if(flag && event.getState() == EState.STATE_NORMAL.ordinal()) event.setState(EState.STATE_BLOCK.ordinal());
            if(!flag && event.getState() == EState.STATE_BLOCK.ordinal()) event.setState(EState.STATE_NORMAL.ordinal());
        });

        return true;
    }


    /**
     * Возвращаем список всех пользователей модераторов
     */
    @Transactional
    public List<User> getUsersIsModerator() {
        return userDao.getUsersIsModerator();
    }


    @Transactional
    public User getUserByPrincipal(Principal principal) {
        if(principal == null) return null;
        String username = principal.getName();
        User user = userDao.getUserOfEmail(username);
        return user;
    }




}
