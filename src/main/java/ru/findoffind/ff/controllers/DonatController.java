package ru.findoffind.ff.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.findoffind.ff.dto.ClipDto;
import ru.findoffind.ff.entity.donation.Donat;
import ru.findoffind.ff.payload.reponse.MessageResponse;
import ru.findoffind.ff.payload.request.DonatCreateRequest;
import ru.findoffind.ff.payload.request.EventSeasonCreateRequest;
import ru.findoffind.ff.services.DonatService;
import ru.findoffind.ff.validations.ResponseErrorValidation;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/api/donat")
public class DonatController {

    @Autowired
    private DonatService donatService;

    @Autowired
    private ResponseErrorValidation responseErrorValidation;

    /**
     * Создаём или дополняем донат
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @PostMapping("/add")
    public ResponseEntity<Object> createOrAddDonat(@Valid @RequestBody DonatCreateRequest createRequest,
                                                    BindingResult bindingResult,
                                                    Principal principal) {

        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        if(donatService.createOrAddDonat(createRequest, principal)) return new ResponseEntity<>(new MessageResponse("donat created"), HttpStatus.OK);
        else return new ResponseEntity<>(new MessageResponse("donat creating error"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Создаём или дополняем донат
     */
    @GetMapping("/all")
    public ResponseEntity<Object> getAllDonats() {

        List<Donat> donats = donatService.getAllDonats();
        if(donats != null){
            return new ResponseEntity<>(donats, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("donat list not get"), HttpStatus.BAD_REQUEST);
    }
}
