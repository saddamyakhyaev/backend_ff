package ru.findoffind.ff.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.findoffind.ff.dto.ClipDto;
import ru.findoffind.ff.dto.UserDto;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.clip.ClipType;
import ru.findoffind.ff.facade.ClipFacade;
import ru.findoffind.ff.payload.reponse.MessageResponse;
import ru.findoffind.ff.payload.request.ClipCreateRequest;
import ru.findoffind.ff.payload.request.SignupRequest;
import ru.findoffind.ff.services.ClipService;
import ru.findoffind.ff.validations.ResponseErrorValidation;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/api/clip")
public class ClipController {

    @Autowired
    private ClipService clipService;

    @Autowired
    private ClipFacade clipFacade;

    @Autowired
    private ResponseErrorValidation responseErrorValidation;


    /**
     * Создаем или меняем закреп
     * @return - результат
     */
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_2')")
    @PostMapping("/create")
    public ResponseEntity<Object> createClip(@Valid @RequestBody ClipCreateRequest createRequest,
                                             BindingResult bindingResult,
                                             Principal principal) {

        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        if(clipService.createClip(createRequest, principal)) return new ResponseEntity<>(new MessageResponse("clip created"), HttpStatus.OK);
        else return new ResponseEntity<>(new MessageResponse("clip creating error"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Возвращает нужный закреп
     * @param clipId закреп
     * @return нужный закреп
     */
    @GetMapping("/{clipId}")
    public ResponseEntity<Object> getClip(@PathVariable Long clipId) {

        Clip clip = clipService.getClipById(clipId);
        if(clip != null){
            ClipDto clipDto = clipFacade.getClipDto(clip);
            return new ResponseEntity<>(clipDto, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("not get clip"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Возвращаем все закрепы
     */
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_3')")
    @GetMapping("/get/all")
    public ResponseEntity<Object> getAllClips() {

        List<Clip> clip = clipService.getAllClips();
        if(clip != null){
            List<ClipDto> clipDtos = clip.stream().map(clipFacade::getClipDto).collect(Collectors.toList());
            return new ResponseEntity<>(clipDtos, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("not get all clip"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Возвращаем все закрепы созданные определенным пользователем
     * @param userId пользователя
     * @return нужные закрепы
     */
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_2')")
    @GetMapping("get/creating/{userId}")
    public ResponseEntity<Object> getClipsCreatingByUser(@PathVariable Long userId,
                                                         Principal principal) {

        List<Clip> clip = clipService.getClipsCreatingByUser(userId);
        if(clip != null){
            List<ClipDto> clipDtos = clip.stream().map(clipFacade::getClipDto).collect(Collectors.toList());
            return new ResponseEntity<>(clipDtos, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("not get clip"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Добавляем закреп в список избранных
     * @param clipId закреп
     */
    @PreAuthorize("isAuthenticated()")
    @GetMapping("add/favourite/{clipId}")
    public ResponseEntity<Object> addInFavouritesClip(@PathVariable Long clipId,
                                                      Principal principal) {

        if(clipService.addOrDeleteInFavouritesClip(true, clipId, principal)){
            return new ResponseEntity<>(new MessageResponse("Clip add in favorites list"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("Clip not add in favorites list"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Удаляем закреп из списка избранных
     * @param clipId закреп
     */
    @PreAuthorize("isAuthenticated()")
    @GetMapping("delete/favourite/{clipId}")
    public ResponseEntity<Object> deleteInFavouritesClip(@PathVariable Long clipId,
                                                      Principal principal) {

        if(clipService.addOrDeleteInFavouritesClip(false, clipId, principal)){
            return new ResponseEntity<>(new MessageResponse("Clip delete in favorites list"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("Clip not delete in favorites list"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Получаем все типы закрепов
     * @return - типы закрепов
     */
    @GetMapping("/types/all")
    public ResponseEntity<Object> getAllClipTypes() {

        List<ClipType> clipTypeList = clipService.getClipTypes();
        if(clipTypeList != null){
            return new ResponseEntity<>(clipTypeList, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("not clip types"), HttpStatus.BAD_REQUEST);
    }



    /**
     * Добавляем главный закреп
     * @param clipId закреп
     */
    @PreAuthorize("isAuthenticated()")
    @GetMapping("add/main/{clipId}")
    public ResponseEntity<Object> addMainClip(@PathVariable Long clipId,
                                                      Principal principal) {

        if(clipService.addMainClip(clipId, principal)){
            return new ResponseEntity<>(new MessageResponse("Clip add as Main list"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("Clip not add as Main list"), HttpStatus.BAD_REQUEST);
    }


}
