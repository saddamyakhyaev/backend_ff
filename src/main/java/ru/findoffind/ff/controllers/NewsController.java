package ru.findoffind.ff.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.findoffind.ff.dto.UserDto;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.event.EventSeason;
import ru.findoffind.ff.entity.news.News;
import ru.findoffind.ff.entity.news.NewsBody;
import ru.findoffind.ff.payload.reponse.MessageResponse;
import ru.findoffind.ff.payload.request.ClipCreateRequest;
import ru.findoffind.ff.payload.request.NewsCreateRequest;
import ru.findoffind.ff.services.NewsService;
import ru.findoffind.ff.validations.ResponseErrorValidation;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/news")
public class NewsController {

    @Autowired
    private NewsService newsService;

    @Autowired
    private ResponseErrorValidation responseErrorValidation;

    /**
     * Создаем или обновляем новость
     * @return - результат
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @PostMapping("/create")
    public ResponseEntity<Object> createNews(@Valid @RequestBody NewsCreateRequest createRequest,
                                             BindingResult bindingResult,
                                             Principal principal) {

        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        if(newsService.createNews(createRequest, principal)) return new ResponseEntity<>(new MessageResponse("news created"), HttpStatus.OK);
        else return new ResponseEntity<>(new MessageResponse("news creating error"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Получаем все активные новости
     * @return - сезоны событий
     */
    @GetMapping("/all")
    public ResponseEntity<Object> getAllEventSeasons() {

        List<News> news = newsService.getAllNews();
        if(news != null){
            news.sort(News::compareTo);
            return new ResponseEntity<>(news, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("not news"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Закрепляем новость
     * @param newsId id новости
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("/is-pinned/{newsId}")
    public ResponseEntity<Object> isPinnedNews(@PathVariable Long newsId,
                                              Principal principal) {

        if(newsService.isPinnedOrNotNews(true, newsId, principal)){
            return new ResponseEntity<>(new MessageResponse("true"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("false"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Открепляем новость
     * @param newsId id новости
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("/is-not-pinned/{newsId}")
    public ResponseEntity<Object> notIsPinnedNews(@PathVariable Long newsId,
                                               Principal principal) {

        if(newsService.isPinnedOrNotNews(false, newsId, principal)){
            return new ResponseEntity<>(new MessageResponse("true"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("false"), HttpStatus.BAD_REQUEST);
    }


    /**
     * Возвращаем тело новости
     * @param newsId - id Новости
     * @return
     */
    @GetMapping("/body/{newsId}")
    public ResponseEntity<Object> getNewsBody(@PathVariable Long newsId) {

        NewsBody newsBody = newsService.getNewsBody(newsId);
        if(newsBody != null){
            return new ResponseEntity<>(newsBody, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("not get newsBody"), HttpStatus.BAD_REQUEST);
    }


    /**
     * Активизируем новость
     * @param newsId id новости
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("/is-active/{newsId}")
    public ResponseEntity<Object> isActiveNews(@PathVariable Long newsId,
                                               Principal principal) {

        if(newsService.isActiveOrNotNews(true, newsId, principal)){
            return new ResponseEntity<>(new MessageResponse("true"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("false"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Убераем активность новости
     * @param newsId id новости
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("/is-not-active/{newsId}")
    public ResponseEntity<Object> notIsActiveNews(@PathVariable Long newsId,
                                                  Principal principal) {

        if(newsService.isActiveOrNotNews(false, newsId, principal)){
            return new ResponseEntity<>(new MessageResponse("true"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("false"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Сохраняем последнюю новость, что видел пользователь
     * @param newsId id новости
     */
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/last-news/{newsId}")
    public ResponseEntity<Object> setUserLastNews(@PathVariable Long newsId,
                                                  Principal principal) {
        if(newsService.setUserLastNews(newsId, principal)){
            return new ResponseEntity<>(new MessageResponse("true"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("false"), HttpStatus.BAD_REQUEST);
    }


}
