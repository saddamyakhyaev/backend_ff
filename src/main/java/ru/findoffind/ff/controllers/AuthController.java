package ru.findoffind.ff.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.enums.ERole;
import ru.findoffind.ff.facade.UserFacade;
import ru.findoffind.ff.payload.reponse.JWTTokenSuccessResponse;
import ru.findoffind.ff.payload.reponse.MessageResponse;
import ru.findoffind.ff.payload.request.LoginRequest;
import ru.findoffind.ff.payload.request.SignupRequest;
import ru.findoffind.ff.security.JWTTokenProvider;
import ru.findoffind.ff.security.SecurityConstants;
import ru.findoffind.ff.security.recaptcha.RecaptchaService;
import ru.findoffind.ff.services.UserService;
import ru.findoffind.ff.validations.ResponseErrorValidation;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/api/auth")
@PreAuthorize("permitAll()")
public class AuthController {

    @Autowired
    private JWTTokenProvider jwtTokenProvider;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private ResponseErrorValidation responseErrorValidation;
    @Autowired
    private UserService userService;
    @Autowired
    private RecaptchaService recaptchaService;
    @Autowired
    private UserFacade userFacade;

    @PostMapping("/signin")
    public ResponseEntity<Object> authenticateUser(@Valid @RequestBody LoginRequest loginRequest,
                                                   BindingResult bindingResult,
                                                   HttpServletRequest request) {
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;


        // Проверяем ReCaptcha если нет Bot Key
        if(loginRequest.getBotKey() == null || !loginRequest.getBotKey().equals("ceca6xcvn65c63f431c0bd84880348bf1f4481d4300aa598719b91853b545cd18")) {

            if(!recaptchaService.check(loginRequest, request))
                return ResponseEntity.badRequest()
                        .body(new MessageResponse("ReCaptcha Error"));
        }

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                loginRequest.getUsername(),
                loginRequest.getPassword()
        ));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = SecurityConstants.TOKEN_PREFIX + jwtTokenProvider.generateToken(authentication);
        User user = userService.getUserById(((User) authentication.getPrincipal()).getId());

        JWTTokenSuccessResponse jwtTokenSuccessResponse = new JWTTokenSuccessResponse(true, jwt, userFacade.getUserDto(user));

        return ResponseEntity.ok(jwtTokenSuccessResponse);
    }


    @PostMapping("/signup")
    public ResponseEntity<Object> registerUser(@Valid @RequestBody SignupRequest signupRequest, BindingResult bindingResult,
                                               HttpServletRequest request) {
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        if(!recaptchaService.check(signupRequest, request))
            return ResponseEntity.badRequest()
                    .body(new MessageResponse("ReCaptcha Error"));

        if(userService.createUser(signupRequest) == null) return new ResponseEntity<>(new MessageResponse("User registered error!"), HttpStatus.BAD_REQUEST);
        else return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

}
