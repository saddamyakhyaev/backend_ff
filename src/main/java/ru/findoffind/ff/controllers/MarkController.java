package ru.findoffind.ff.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.findoffind.ff.entity.event.EventLimit;
import ru.findoffind.ff.entity.mark.Mark;
import ru.findoffind.ff.payload.reponse.MessageResponse;
import ru.findoffind.ff.payload.request.EventLimitCreateRequest;
import ru.findoffind.ff.payload.request.MarkCreateRequest;
import ru.findoffind.ff.services.MarkService;
import ru.findoffind.ff.services.NewsService;
import ru.findoffind.ff.validations.ResponseErrorValidation;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/mark")
public class MarkController {

    @Autowired
    private MarkService markService;

    @Autowired
    private ResponseErrorValidation responseErrorValidation;

    /**
     * Получаем все марки
     */
    @GetMapping("/all")
    public ResponseEntity<Object> getAllMark() {

        List<Mark> marks = markService.getAllMark();
        if(marks != null){
            return new ResponseEntity<>(marks, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("not marks"), HttpStatus.BAD_REQUEST);
    }


    /**
     * Создаём марку
     */
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_3')")
    @PostMapping("create")
    public ResponseEntity<Object> createMark(@Valid @RequestBody MarkCreateRequest createRequest,
                                                   BindingResult bindingResult,
                                                   Principal principal) {

        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        if(markService.createMark(createRequest, principal)) return new ResponseEntity<>(new MessageResponse("event limit created"), HttpStatus.OK);
        else return new ResponseEntity<>(new MessageResponse("mark creating error"), HttpStatus.BAD_REQUEST);
    }
}
