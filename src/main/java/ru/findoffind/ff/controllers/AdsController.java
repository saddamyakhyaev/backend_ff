package ru.findoffind.ff.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.findoffind.ff.entity.ads.Ads;
import ru.findoffind.ff.entity.ads.AdsBody;
import ru.findoffind.ff.entity.news.News;
import ru.findoffind.ff.entity.news.NewsBody;
import ru.findoffind.ff.payload.reponse.MessageResponse;
import ru.findoffind.ff.payload.request.AdsCreateRequest;
import ru.findoffind.ff.payload.request.NewsCreateRequest;
import ru.findoffind.ff.services.AdsService;
import ru.findoffind.ff.services.NewsService;
import ru.findoffind.ff.validations.ResponseErrorValidation;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/ads")
public class AdsController {

    @Autowired
    private AdsService adsService;

    @Autowired
    private ResponseErrorValidation responseErrorValidation;

    /**
     * Создаем рекламу
     * @return - результат
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @PostMapping("/create")
    public ResponseEntity<Object> createAds(@Valid @RequestBody AdsCreateRequest createRequest,
                                             BindingResult bindingResult,
                                             Principal principal) {

        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        if(adsService.createAds(createRequest, principal)) return new ResponseEntity<>(new MessageResponse("ads created"), HttpStatus.OK);
        else return new ResponseEntity<>(new MessageResponse("ads creating error"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Получаем все активные рекламы
     * @return - список реклам
     */
    @GetMapping("/all")
    public ResponseEntity<Object> getAllEventAds() {

        List<Ads> ads = adsService.getAllAds();
        if(ads != null){
            return new ResponseEntity<>(ads, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("not ads"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Возвращаем тело рекламы
     * @param adsId - id рекламы
     * @return
     */
    @GetMapping("/body/{adsId}")
    public ResponseEntity<Object> getNewsBody(@PathVariable Long adsId) {

        AdsBody adsBody = adsService.getAdsBody(adsId);
        if(adsBody != null){
            return new ResponseEntity<>(adsBody, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("not get ads Body"), HttpStatus.BAD_REQUEST);
    }


    /**
     * Активизируем рекламу
     * @param adsId id рекламы
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("/is-active/{adsId}")
    public ResponseEntity<Object> isActiveAds(@PathVariable Long adsId,
                                               Principal principal) {

        if(adsService.isActiveOrNotAds(true, adsId, principal)){
            return new ResponseEntity<>(new MessageResponse("true"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("false"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Убираем активность рекламы
     * @param adsId id рекламы
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("/is-not-active/{adsId}")
    public ResponseEntity<Object> notIsActiveNews(@PathVariable Long adsId,
                                                  Principal principal) {

        if(adsService.isActiveOrNotAds(false, adsId, principal)){
            return new ResponseEntity<>(new MessageResponse("true"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("false"), HttpStatus.BAD_REQUEST);
    }
}
