package ru.findoffind.ff.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.findoffind.ff.dto.FindDto;
import ru.findoffind.ff.dto.UserDto;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.payload.reponse.MessageResponse;
import ru.findoffind.ff.services.FindService;

import java.security.Principal;

@CrossOrigin
@RestController
@RequestMapping("/api/find")
public class FindController {


    @Autowired
    private FindService findService;


    @GetMapping("/{str}")
    public ResponseEntity<Object> getCurrentUser(
            @PathVariable String str,
            Principal principal) {

        FindDto find = findService.find(str, principal);

        if(find == null) return new ResponseEntity<>(new MessageResponse("not finding"), HttpStatus.BAD_REQUEST);
        else return new ResponseEntity<>(find, HttpStatus.OK);
    }
}
