package ru.findoffind.ff.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.findoffind.ff.dto.EventDto;
import ru.findoffind.ff.dto.UserDto;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.enums.ERole;
import ru.findoffind.ff.entity.event.Event;
import ru.findoffind.ff.facade.UserFacade;
import ru.findoffind.ff.payload.reponse.MessageResponse;
import ru.findoffind.ff.services.UserService;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserFacade userFacade;

    //@PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/")
    public ResponseEntity<UserDto> getCurrentUser(Principal principal) {
        User user = userService.getCurrentUser(principal);
        UserDto userDTO = userFacade.getUserDto(user);
        userDTO.setUserBox(userService.getUserBoxById(user.getId()));

        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    //@PreAuthorize("hasAuthority('ROLE_ADMIN') || hasAuthority('ROLE_MODERATOR_3')")
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_2')")
    @GetMapping("/get/{userId}")
    public ResponseEntity<Object> getUser(@PathVariable Long userId,
                                          Principal principal) {

        User user = userService.getUserById(userId);
        if(user != null){
            UserDto userDTO = userFacade.getUserDto(user);
            return new ResponseEntity<>(userDTO, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("not get user"), HttpStatus.BAD_REQUEST);
    }

    @PreAuthorize("hasAuthority('ROLE_MODERATOR_2')")
    @GetMapping("/role/set/{userId}/{role}")
    public ResponseEntity<Object> setRoleUser(@PathVariable Long userId,
                                              @PathVariable ERole role,
                                          Principal principal) {

        if(userService.setRoleUser(userId, role, principal)){
            return new ResponseEntity<>(new MessageResponse("user set role"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("user not role set"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Блокируем пользователя
     * @param userId - id пользователя
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_3')")
    @PostMapping("/block-user/{userId}")
    public ResponseEntity<Object> getBlockingUser(@PathVariable Long userId,
                                          Principal principal) {
        if(userService.getBlockingUser(true, userId, principal)){
            return new ResponseEntity<>(new MessageResponse("user blocked"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("user not blocked"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Разблокируем пользователя
     * @param userId - id пользователя
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_3')")
    @PostMapping("/un-block-user/{userId}")
    public ResponseEntity<Object> getUnBlockingUser(@PathVariable Long userId,
                                                  Principal principal) {
        if(userService.getBlockingUser(false, userId, principal)){
            return new ResponseEntity<>(new MessageResponse("user blocked"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("user not blocked"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Возвращаем список всех пользователей модераторов
     */
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_3')")
    @GetMapping("/get/moderators/")
    public ResponseEntity<Object> getUsersIsModerator(Principal principal) {

        List<User> users = userService.getUsersIsModerator();
        if(users != null){
            List<UserDto> userDtos = users.stream()
                    .map(userFacade::getUserDto)
                    .collect(Collectors.toList());
            return new ResponseEntity<>(userDtos, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("not users limit"), HttpStatus.BAD_REQUEST);
    }



}
