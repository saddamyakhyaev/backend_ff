package ru.findoffind.ff.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.findoffind.ff.dto.EventDto;
import ru.findoffind.ff.entity.clip.ClipType;
import ru.findoffind.ff.entity.event.Event;
import ru.findoffind.ff.entity.event.EventLimit;
import ru.findoffind.ff.entity.event.EventSeason;
import ru.findoffind.ff.facade.EventFacade;
import ru.findoffind.ff.payload.reponse.MessageResponse;
import ru.findoffind.ff.payload.request.ClipCreateRequest;
import ru.findoffind.ff.payload.request.EventCreateRequest;
import ru.findoffind.ff.payload.request.EventLimitCreateRequest;
import ru.findoffind.ff.payload.request.EventSeasonCreateRequest;
import ru.findoffind.ff.services.EventService;
import ru.findoffind.ff.validations.ResponseErrorValidation;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/api/event")
public class EventController {

    @Autowired
    private EventService eventService;

    @Autowired
    private EventFacade eventFacade;

    @Autowired
    private ResponseErrorValidation responseErrorValidation;


    /**
     * Создаём сезон событий
     * @return - результат
     */
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_3')")
    @PostMapping("/season/create")
    public ResponseEntity<Object> createEventSeason(@Valid @RequestBody EventSeasonCreateRequest createRequest,
                                             BindingResult bindingResult,
                                             Principal principal) {

        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        EventSeason eventSeason = eventService.createEventSeason(createRequest, principal);

        if(eventSeason != null) return new ResponseEntity<>(eventSeason, HttpStatus.OK);
        else return new ResponseEntity<>(new MessageResponse("event season creating error"), HttpStatus.BAD_REQUEST);
    }



    /**
     * Получаем все сезоны событий
     * @return - сезоны событий
     */
    @GetMapping("/season/all")
    public ResponseEntity<Object> getAllEventSeasons() {

        List<EventSeason> eventSeasons = eventService.getAllEventSeasons();
        if(eventSeasons != null){
            return new ResponseEntity<>(eventSeasons, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("not event season"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Активируем сезон
     */
    @PostMapping("/season/active/{eventSeasonId}")
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_3')")
    public ResponseEntity<Object> activeEventSeason(@PathVariable Long eventSeasonId) {

        if(eventService.activeOrBlockEventSeason(eventSeasonId, true)){
            return new ResponseEntity<>(new MessageResponse("event season active"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("event season active - error"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Блокируем сезон
     */
    @PostMapping("/season/block/{eventSeasonId}")
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_3')")
    public ResponseEntity<Object> blockEventSeason(@PathVariable Long eventSeasonId) {

        if(eventService.activeOrBlockEventSeason(eventSeasonId, false)){
            return new ResponseEntity<>(new MessageResponse("event season block"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("event season blocking - error"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Удалить сезоны
     * @return - сезоны событий
     */
    @PostMapping("/season/delete/{eventSeasonId}")
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_3')")
    public ResponseEntity<Object> deleteEventSeason(@PathVariable Long eventSeasonId) {

        if(eventService.deleteEventSeason(eventSeasonId)){
            return new ResponseEntity<>(new MessageResponse("event limit deleted"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("event limit not delete"), HttpStatus.BAD_REQUEST);
    }



    /**
     * Создаём границы событий
     * @return - результат
     */
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_3')")
    @PostMapping("/limit/create")
    public ResponseEntity<Object> createEventLimit(@Valid @RequestBody EventLimitCreateRequest createRequest,
                                                    BindingResult bindingResult,
                                                    Principal principal) {

        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        if(eventService.createEventLimit(createRequest, principal)) return new ResponseEntity<>(new MessageResponse("event limit created"), HttpStatus.OK);
        else return new ResponseEntity<>(new MessageResponse("event limit creating error"), HttpStatus.BAD_REQUEST);
    }



    /**
     * Получаем все лимиты событий
     * @return - сезоны событий
     */
    @GetMapping("/limit/all")
    public ResponseEntity<Object> getAllEventLimits() {

        List<EventLimit> eventSeasons = eventService.getAllEventLimits();
        if(eventSeasons != null){
            return new ResponseEntity<>(eventSeasons, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("not event limit"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Удалить лимит
     * @return - сезоны событий
     */
    @PostMapping("/limit/delete/{eventLimitId}")
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_3')")
    public ResponseEntity<Object> deleteEventLimit(@PathVariable Long eventLimitId) {

        if(eventService.deleteEventLimit(eventLimitId)){
            return new ResponseEntity<>(new MessageResponse("event limit deleted"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("event limit not delete"), HttpStatus.BAD_REQUEST);
    }


    /**
     * Создаём событие
     * @return - результат
     */
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_2')")
    @PostMapping("/create")
    public ResponseEntity<Object> createEvent(@Valid @RequestBody EventCreateRequest createRequest,
                                                    BindingResult bindingResult,
                                                    Principal principal) {

        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        if(eventService.createEvent(createRequest, principal)) return new ResponseEntity<>(new MessageResponse("event created"), HttpStatus.OK);
        else return new ResponseEntity<>(new MessageResponse("event creating error"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Получаем все события по закрепу
     * @param clipId id закрепа
     * @return
     */
    @GetMapping("/all/{clipId}")
    public ResponseEntity<Object> getAllEvent(@PathVariable Long clipId) {

        List<Event> events = eventService.getAllEventByClip(clipId);
        if(events != null){
            List<EventDto> eventDtos = events.stream()
                    .map(eventFacade::getEventDto)
                    .collect(Collectors.toList());
            return new ResponseEntity<>(eventDtos, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("not event limit"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Получаем все события по закрепу определенного сезона
     */
    @GetMapping("/all/{clipId}/{seasonId}")
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_3')")
    public ResponseEntity<Object> getAllEventBySeason(@PathVariable Long clipId,
                                                      @PathVariable Long seasonId) {

        List<Event> events = eventService.getAllEventByClipAndBySeason(clipId, seasonId);
        if(events != null){
            List<EventDto> eventDtos = events.stream()
                    .map(eventFacade::getEventDto)
                    .collect(Collectors.toList());
            return new ResponseEntity<>(eventDtos, HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("not event limit"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Удалить событие
     * @return - сезоны событий
     */
    @PostMapping("/delete/{eventId}")
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_2')")
    public ResponseEntity<Object> deleteEvent(@PathVariable Long eventId, Principal principal) {

        if(eventService.deleteEvent(eventId, principal)){
            return new ResponseEntity<>(new MessageResponse("event deleted"), HttpStatus.OK);
        }else return new ResponseEntity<>(new MessageResponse("event not delete"), HttpStatus.BAD_REQUEST);
    }
}
