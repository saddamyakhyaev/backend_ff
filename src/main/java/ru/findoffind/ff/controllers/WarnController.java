package ru.findoffind.ff.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.findoffind.ff.dto.EventDto;
import ru.findoffind.ff.entity.event.Event;
import ru.findoffind.ff.entity.warn.*;
import ru.findoffind.ff.payload.reponse.MessageResponse;
import ru.findoffind.ff.payload.request.NewsCreateRequest;
import ru.findoffind.ff.payload.request.WarnCreateRequest;
import ru.findoffind.ff.security.recaptcha.RecaptchaService;
import ru.findoffind.ff.services.WarnService;
import ru.findoffind.ff.validations.ResponseErrorValidation;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/api/warn")
public class WarnController {

    @Autowired
    private WarnService warnService;

    @Autowired
    private ResponseErrorValidation responseErrorValidation;

    @Autowired
    private RecaptchaService recaptchaService;

    /**
     * Оставляем ошибку
     *
     * @return - результат
     */
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/send")
    public ResponseEntity<Object> sendWarn(@Valid @RequestBody WarnCreateRequest createRequest,
                                           BindingResult bindingResult,
                                           HttpServletRequest request,
                                           Principal principal) {

        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        if (!recaptchaService.check(createRequest, request))
            return ResponseEntity.badRequest()
                    .body(new MessageResponse("ReCaptcha Error"));

        if (warnService.sendWarn(createRequest, principal))
            return new ResponseEntity<>(new MessageResponse("warn created"), HttpStatus.OK);
        else return new ResponseEntity<>(new MessageResponse("warn creating error"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Получаем последние предупреждения
     */
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_3')")
    @GetMapping("/last")
    public ResponseEntity<Object> getLastWarns() {

        List<Warn> warns = warnService.getLastWarns();
        if (warns != null) {
            warns.sort(Warn::compareTo);
            return new ResponseEntity<>(warns, HttpStatus.OK);
        } else return new ResponseEntity<>(new MessageResponse("not last warns limit"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Очищаем все предупреждения
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("/clear")
    public ResponseEntity<Object> clearAllWarn() {

        if (warnService.clearAllWarn()) {
            return new ResponseEntity<>(new MessageResponse("warns cleare"), HttpStatus.OK);
        } else return new ResponseEntity<>(new MessageResponse("not warns cleare"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Получаем последние предупреждения по пользователю
     */
    @PreAuthorize("hasAuthority('ROLE_MODERATOR_3')")
    @GetMapping("/user/{userId}")
    public ResponseEntity<Object> getLastWarnsByUser(@PathVariable Long userId) {
        List<Warn> warns = warnService.getLastWarnsByUser(userId);
        if (warns != null) {
            warns.sort(Warn::compareTo);
            return new ResponseEntity<>(warns, HttpStatus.OK);
        } else return new ResponseEntity<>(new MessageResponse("not last warns limit"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Очищаем предупреждения по пользователю
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("/user/clear/{userId}")
    public ResponseEntity<Object> clearLastWarnsByUser(@PathVariable Long userId) {

        if (warnService.clearLastWarnsByUser(userId)) {
            return new ResponseEntity<>(new MessageResponse("warns cleare"), HttpStatus.OK);
        } else return new ResponseEntity<>(new MessageResponse("not warns cleare"), HttpStatus.BAD_REQUEST);
    }
}
