package ru.findoffind.ff.facade;

import org.springframework.stereotype.Component;
import ru.findoffind.ff.dto.ClipDto;
import ru.findoffind.ff.entity.clip.Clip;

/**
 * Преобразует сущности для передачи на клиент:
 *  - {@link Clip} - {@link ClipDto}
 */
@Component
public class ClipFacade {

    /**
     * Подготавливаем закрепы для клиента
     * @param clip - сущность закрепа
     * @return обьект dto
     */
    public ClipDto getClipDto(Clip clip) {

        ClipDto clipDto = new ClipDto();
        clipDto.setId(clip.getId());
        clipDto.setName(clip.getName());
        clipDto.setTypeClipId(clip.getTypeClipId());
        clipDto.setDescription(clip.getDescription());
        clipDto.setInformation(clip.getInformation());
        clipDto.setState(clip.getState());
        clipDto.setMarkId(clip.getMarkId());

        return clipDto;
    }
}
