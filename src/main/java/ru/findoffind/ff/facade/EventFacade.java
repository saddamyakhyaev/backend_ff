package ru.findoffind.ff.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.findoffind.ff.dto.ClipDto;
import ru.findoffind.ff.dto.EventDto;
import ru.findoffind.ff.entity.event.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Преобразует сущности для передачи на клиент:
 *  - {@link Event} - {@link EventDto}
 */
@Component
public class EventFacade {

    @Autowired
    private ClipFacade clipFacade;

    public EventDto getEventDto(Event event){

        EventDto eventDto = new EventDto();
        eventDto.setId(event.getId());
        eventDto.setDescription(event.getDescription());
        eventDto.setInformation(event.getInformation());

        List<ClipDto> clips = event.getClips().stream().map(clipFacade::getClipDto).collect(Collectors.toList());
        eventDto.setClips(clips);

        eventDto.setDates(event.getTimes());
        eventDto.setSeason(event.getSeason());
        eventDto.setCreateUserId(event.getCreateUserId());
        eventDto.setCreatedDate(event.getCreatedDate());

        return eventDto;
    }
}
