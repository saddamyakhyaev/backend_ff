package ru.findoffind.ff.facade;

import org.springframework.stereotype.Component;
import ru.findoffind.ff.dto.UserDto;
import ru.findoffind.ff.entity.User;

/**
 * Преобразует сущности для передачи на клиент:
 *  - {@link User} - {@link UserDto}
 */
@Component
public class UserFacade {

    /**
     * Подготавливаем пользовательсие данные для клиента
     * @param user - сущность пользователя
     * @return обьект dto
     */
    public UserDto getUserDto(User user){

        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setProtectedId(user.getProtectedId());
        userDto.setRole(user.getRole());
        userDto.setEmail(user.getEmail());
        userDto.setState(user.getState());

        return userDto;
    }
}
