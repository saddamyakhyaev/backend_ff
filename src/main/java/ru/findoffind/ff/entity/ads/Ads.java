package ru.findoffind.ff.entity.ads;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.enums.ERole;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Главный класс для работы рекламы
 */
@Data
@Entity
@Table(name="ads")
public class Ads {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Заголовок рекламы
     */
    @Column(nullable = false)
    private String name;

    /**
     * Описание рекламы
     */
    @Column(nullable = false)
    private String description;


    /**
     * Ссылка на ресурс
     */
    @Column(columnDefinition = "text")
    private String link;

    /**
     * Главная картинка рекламы для ПК
     */
    @Column(columnDefinition = "text")
    private String imageUrlPc;

    /**
     * Главная картинка рекламы для Мобильных устройств
     */
    @Column(columnDefinition = "text")
    private String imageUrlMobil;


    /**
     * Дата с которой будет отображаться реклама
     */
    @Column(nullable = false)
    private LocalDateTime dateFrom;

    /**
     * Дата до которой будет отображаться реклама
     */
    @Column(nullable = false)
    private LocalDateTime dateTo;

    /**
     * Активность рекламы
     */
    @Column(nullable = false)
    private Boolean isActive;

    /**
     * Пользователь создавший данную рекламу
     * Должен иметь права:
     * - Равно или выше модератора 3 уровня {@link ERole#ROLE_MODERATOR_3}
     */
    @OneToOne
    private User createUser;

    /**
     * Дата создания реламы
     */
    @JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss")
    @Column(updatable = false)
    private LocalDateTime createdDate;

    @PrePersist
    protected void onCreate() {
        this.createdDate = LocalDateTime.now();
    }
}
