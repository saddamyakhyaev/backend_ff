package ru.findoffind.ff.entity.ads;

import lombok.Data;

import javax.persistence.*;

/**
 * Тело рекламы
 */
@Data
@Entity
@Table(name="adsbody")
public class AdsBody {

    @Id
    private Long id;

    @Column(nullable = false, columnDefinition = "text")
    @Basic(fetch = FetchType.LAZY)
    private String text;
}
