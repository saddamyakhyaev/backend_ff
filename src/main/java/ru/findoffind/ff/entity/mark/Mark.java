package ru.findoffind.ff.entity.mark;

import lombok.Data;
import ru.findoffind.ff.entity.clip.Clip;

import javax.persistence.*;

/**
 * Маркировка, пока что только у {@link Clip}
 */
@Data
@Entity
@Table(name="mark")
public class Mark {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Названия марки
     */
    @Column(nullable = false)
    private String name;

    /**
     * Id марки родителя если такая существует
     */
    @Column
    private Long parentMarkId;
}
