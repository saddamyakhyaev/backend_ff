package ru.findoffind.ff.entity.donation;

import lombok.Data;
import ru.findoffind.ff.entity.clip.Clip;

import javax.persistence.*;

@Data
@Entity
@Table(name="donat")
public class Donat {

    @Id
    private Long id;

    @Column
    private Integer coins;

    @OneToOne(fetch = FetchType.EAGER)
    @MapsId
    private Clip clip;

}
