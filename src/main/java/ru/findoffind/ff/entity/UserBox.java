package ru.findoffind.ff.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.event.EventTime;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name="userbox")
public class UserBox {

    @Id
    @JsonIgnore
    private Long id;


    @Column
    private Long lastNews;

    /**
     * Список закрепов добавленных в избранное
     */
    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private List<Clip> favouritesClips = new ArrayList<>();

    /**
     * Главный закреп
     */
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Clip mainClip;

    /**
     * Деньги что задонатил пользователь
     */
    @Column
    private Integer coins;


    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private User user;
}
