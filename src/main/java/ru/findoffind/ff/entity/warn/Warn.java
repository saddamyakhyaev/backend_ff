package ru.findoffind.ff.entity.warn;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Данный класс хранит ошибку от пользователя или наша личная ошибка
 */
@Data
@Entity
@Table(name="warn")
public class Warn implements Comparable<Warn> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Текст ошибки
     */
    @Column(columnDefinition = "text")
    private String text;

    /**
     * Текст ошибки
     */
    @Column(nullable = false)
    private Integer codeWarn;

    /**
     * Id объекта привязанного к ошибке
     */
    @Column
    private Long objectId;

    /**
     * Тип оъекта привязанного к ошибке
     */
    @Column
    private Integer objectType;

    /**
     * Ссылка на обьект ошибка
     */
    private String objectLink;

    /**
     * Id пользователя связанного с данной ошибкой
     */
    @Column
    private Long userId;

    /**
     * Id Пользователя добавившего данное предупреждение
     */
    @Column
    private Long createUserId;


    @JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss")
    @Column(updatable = false)
    private LocalDateTime createdDate;

    @PrePersist
    protected void onCreate() {
        this.createdDate = LocalDateTime.now();
    }

    @Override
    public int compareTo(Warn o) {
        return o.createdDate.compareTo(createdDate);
    }
}
