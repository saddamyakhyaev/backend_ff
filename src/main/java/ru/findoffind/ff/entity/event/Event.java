package ru.findoffind.ff.entity.event;

import lombok.Data;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.clip.Clip;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * Событие работает в связке с закрепом {@link Clip}
 * и является тем, ради чего
 * пользователь заходит на сервис.
 *
 * Главные поля:
 * - {@link Event#description}
 * - {@link Event#clips}
 * - {@link Event#times}
 */
@Data
@Entity
@Table(name="event")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Описание события - главный текст
     * Должен содержать всю основную информацию о событии
     * Так же может содержать хештеги, для сортировки событий
     */
    @Column(nullable = false,
            columnDefinition = "text")
    private String description;

    /**
     * Тут хранится информация для работы программы
     * Она невидна клиенту
     */
    @Column
    private String information;



    /**
     * Закрепы к которое связанны с данным событием
     */
    @ManyToMany (cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinTable(name = "event_clip",
            joinColumns = @JoinColumn(name = "event_id"),
            inverseJoinColumns = @JoinColumn(name = "clip_id")
    )
    private Set<Clip> clips = new HashSet<>();

    /**
     * Событие может проходить в разное время
     * Данный список содержит сущности
     * для определения дат
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "event_id")
    private Set<EventTime> times = new HashSet<>();

    /**
     * Время создания события
     */
    @Column(nullable = false)
    private LocalDateTime createdDate;

    /**
     * ID пользователя который создал данное событие
     */
    @Column
    private Long createUserId;

    /**
     * Статус закрепа
     */
    @Column
    private Integer state;



    @PrePersist
    protected void onCreate() {
        this.createdDate = LocalDateTime.now();
    }

    /**
     * Сезон к которому привязано событие
     */
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "season_id")
    private EventSeason season;


    public void addClip(Clip clip){
        this.clips.add(clip);
    }

    public void removeClip(Clip clip){
        this.clips.remove(clip);
    }

}
