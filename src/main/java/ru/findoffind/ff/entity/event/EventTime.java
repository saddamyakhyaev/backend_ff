package ru.findoffind.ff.entity.event;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;


/**
 * Сущность которая отвечает
 * за определение времени события {@link Event}
 *
 * Главные поля:
 * - {@link #dates}
 * - {@link #timeFrom}
 * - {@link #timeTo}
 */
@Data
@Entity
@Table(name="eventtime")
public class EventTime {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Определённые дни
     */
    @ElementCollection(targetClass = LocalDate.class)
    @CollectionTable(name = "event_time_dates",
            joinColumns = @JoinColumn(name = "event_id"))
    private Set<LocalDate> dates = new HashSet<>();


    /**
     * Время начала события
     */
    @Column(nullable = false)
    private LocalTime timeFrom;

    /**
     * Время окончания события
     */
    @Column(nullable = false)
    private LocalTime timeTo;
}

