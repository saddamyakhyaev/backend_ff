package ru.findoffind.ff.entity.event;


import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Сезоны позваляют удобно манипулировать событиями {@link Event}
 */
@Data
@Entity
@Table(name="eventseason")
public class EventSeason implements Comparable<EventSeason> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Названия сезона
     * Главное назначение - навигация
     */
    @Column(nullable = false)
    private String name;

    /**
     * Описание сезона событий
     * По большей части для навигации и манипуляции
     */
    @Column(columnDefinition = "text")
    private String description;

    /**
     * Дата с которой все события
     * привязанные к данному сезону
     * будут отображаться пользователю
     */
    @Column(nullable = false)
    private LocalDate dateFrom;

    /**
     * Дата по которою все события
     * привязанные к данному сезону
     * будут не активны
     */
    @Column(nullable = false)
    private LocalDate dateTo;

    /**
     * Статус сезона
     */
    @Column(nullable = false)
    private Integer state;


    @Override
    public int compareTo(EventSeason o) {
        return o.id.compareTo(id);
    }
}

