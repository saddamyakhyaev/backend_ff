package ru.findoffind.ff.entity.event;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Данная сущность хранит в себе
 * границы события в которое проходит событие
 * Например, 14:00 - 15:30 Первая пара
 */
@Data
@Entity
@Table(name="eventlimit")
public class EventLimit implements Comparable<EventLimit> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Название границы
     */
    @Column(unique = true)
    private String name;

    /**
     * Время начала события
     */
    @Column(nullable = false)
    private LocalTime timeFrom;

    /**
     * Время окончания события
     */
    @Column(nullable = false)
    private LocalTime timeTo;


    @Override
    public int compareTo(EventLimit o) {
        return timeFrom.isAfter(o.timeFrom)? 1: -1;
    }
}
