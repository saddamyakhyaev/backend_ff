package ru.findoffind.ff.entity.clip;

import lombok.Data;

import javax.persistence.*;

/**
 * Тип закрепа
 */
@Data
@Entity
@Table(name="cliptype")
public class ClipType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Название типа объекта
     */
    private String name;

    /**
     * Название иконки
     */
    private String icon;
}
