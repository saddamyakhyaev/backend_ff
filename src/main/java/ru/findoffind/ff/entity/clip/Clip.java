package ru.findoffind.ff.entity.clip;

import lombok.Data;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.enums.ERole;
import ru.findoffind.ff.entity.enums.EState;

import javax.persistence.*;

/**
 * Главный объект:
 *  - Участвует в поиске
 *  - Хранит в себе события
 */
@Data
@Entity
@Table(name="clip")
public class Clip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Id типа закрепа {@link ClipType}
     */
    @Column(nullable = false)
    private Long typeClipId;

    /**
     * Название закрепа
     * Должно быть коротким
     */
    @Column(unique = true)
    private String name;

    /**
     * Описание закрепа
     */
    @Column(nullable = false)
    private String description;

    /**
     * Тут хранится информация для работы программы
     * Она невидна клиенту
     */
    @Column
    private String information;

    /**
     * Состояние закрепа
     * Все состояния указаны в {@link EState}
     */
    @Column(nullable = false)
    private Integer state;

    /**
     * Id марки к которой прикреплен закреп
     */
    @Column
    private Long markId;

    /**
     * Количество пользователей у кого это закреплено
     */
    @Column
    private Integer countFavoriteUser;

    /**
     * Количество пользователей у кого это главный Закреп
     */
    @Column
    private Integer countMainUser;



    /**
     * Id Пользователя создавший данный закреп
     * Должен иметь права:
     * - Равно или выше модератора 2 уровня {@link ERole#ROLE_MODERATOR_2}
     */
    @Column
    private Long createUserId;

}
