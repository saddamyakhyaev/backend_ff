package ru.findoffind.ff.entity.enums;

import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.event.Event;
import ru.findoffind.ff.entity.event.EventSeason;

import java.util.function.Predicate;

public enum ERole implements Predicate<ERole> {


    /**
     * Права админа:
     *  - Теоретически - неограничены ничем
     */
    ROLE_ADMIN,


    /**
     * Права модератора 3 уровня:
     *  - Блокировать пользователей любых прав
     *  - Назначать права модератора 2 уровня другим пользователям
     *  - (Всё что может модератор 2 уровня)
     */
    ROLE_MODERATOR_3,

    /**
     * Права модератора 2 уровня:
     *  - Создавать закрепы {@link Clip}
     *  - Создавать события {@link Event}
     *    к созданным им же закрепам
     */
    ROLE_MODERATOR_2,

    /**
     * Права обыкновенного пользователя:
     *  - Просматривать объекты, события
     *  - Закреплять особо важные
     *  - Участвовать в исправлении контента
     */
    ROLE_USER;





    @Override
    public boolean test(ERole eRole) {
        return this == eRole;
    }
}

