package ru.findoffind.ff.entity.enums;

public enum EState {

    STATE_NORMAL,
    STATE_BLOCK,
    STATE_DELETE
}
