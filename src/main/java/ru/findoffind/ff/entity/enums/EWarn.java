package ru.findoffind.ff.entity.enums;

/**
 * Коды ошибок
 */
public enum EWarn {
    /**
     * Универсльная ошибка
     */
    ERROR_ABOUT_UNIVERSAL,

    /**
     * Ошибка связанная с закрепом
     */
    ERROR_ABOUT_CLIP,

    /**
     * Ошибка связанная с событием
     */
    ERROR_ABOUT_EVENT
}
