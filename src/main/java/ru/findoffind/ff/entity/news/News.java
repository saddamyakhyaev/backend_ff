package ru.findoffind.ff.entity.news;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.context.annotation.Lazy;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.enums.ERole;
import ru.findoffind.ff.entity.mark.Mark;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Класс для новостей
 */
@Data
@Entity
@Table(name="news")
public class News implements Comparable<News> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Уникальное название новости
     */
    @Column
    private String nameUnique;

    /**
     * Заголовок новости
     */
    @Column(nullable = false)
    private String name;

    /**
     * Описание новости
     */
    @Column(nullable = false)
    private String description;

    /**
     * Закрепленная новость
     */
    @Column(nullable = false)
    private Boolean isPinned;

    /**
     * Уведомлять пользователей или нет
     */
    @Column(nullable = false)
    private Boolean isNotification;

    /**
     * Активность новости
     */
    @Column(nullable = false)
    private Boolean isActive;

    /**
     * Дата с которой будет отображаться новость
     */
    @Column(nullable = false)
    private LocalDate dateFrom;

    /**
     * Дата до которой будет отображаться новость
     */
    @Column(nullable = false)
    private LocalDate dateTo;

    /**
     * Пользователь создавший данную новость
     * Должен иметь права:
     * - Равно или выше модератора 3 уровня {@link ERole#ROLE_MODERATOR_3}
     */
    @OneToOne
    private User createUser;

    /**
     * Список марок, которым нужно показать новость
     */
    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinTable(name = "news_mark",
            joinColumns = @JoinColumn(name = "news_id"),
            inverseJoinColumns = @JoinColumn(name = "mark_id")
    )
    private Set<Mark> marks = new HashSet<>();


    /**
     * Дата создания новости
     */
    @JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss")
    @Column(updatable = false)
    private LocalDateTime createdDate;

    @PrePersist
    protected void onCreate() {
        this.createdDate = LocalDateTime.now();
    }


    @Override
    public int compareTo(News o) {
        return o.dateFrom.compareTo(dateFrom);
    }
}
