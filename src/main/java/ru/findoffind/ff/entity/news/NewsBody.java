package ru.findoffind.ff.entity.news;

import lombok.Data;

import javax.persistence.*;

/**
 * Данный класс хранит главный текст новости
 */
@Data
@Entity
@Table(name="newsbody")
public class NewsBody {

    @Id
    private Long id;

    @Column(nullable = false, columnDefinition = "text")
    @Basic(fetch = FetchType.LAZY)
    private String text;
}
