package ru.findoffind.ff.payload.reponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.findoffind.ff.dto.UserDto;

@Data
@AllArgsConstructor
public class JWTTokenSuccessResponse {
   private boolean success;
   private String token;
   private UserDto user;
}
