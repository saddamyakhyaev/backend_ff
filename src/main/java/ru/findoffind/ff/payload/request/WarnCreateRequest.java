package ru.findoffind.ff.payload.request;

import lombok.Data;
import ru.findoffind.ff.payload.request.tool.ReCaptchaRequest;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Объект данных для создания предупреждения
 */
@Data
public class WarnCreateRequest extends ReCaptchaRequest {

    @NotEmpty(message = "Text cannot be empty")
    @Size(min = 5, max = 1000)
    private String text;

    @NotNull(message = "CodeWarn cannot be empty")
    private Integer codeWarn;

    private Long objectId;

    private Integer objectType;

    private String objectLink;
}
