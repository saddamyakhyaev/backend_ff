package ru.findoffind.ff.payload.request.tool;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * Для запросов где нужна
 */
@Data
public class ReCaptchaRequest {

    @NotEmpty(message = "Recaptcha cannot be empty")
    protected String recaptcha;

}
