package ru.findoffind.ff.payload.request;

import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * Объект данных для создания марки от клиента
 */
@Data
public class MarkCreateRequest {

    private Long id;

    @NotEmpty(message = "Name cannot be empty")
    @Size(min = 1, max = 100)
    private String name;

    private Long parentMarkId;
}
