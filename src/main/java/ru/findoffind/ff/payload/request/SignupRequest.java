package ru.findoffind.ff.payload.request;

import lombok.Data;
import ru.findoffind.ff.payload.request.tool.ReCaptchaRequest;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


/**
 * Обьект данных для регистрации от клиента
 */
@Data
public class SignupRequest extends ReCaptchaRequest {

    @Email(message = "It should have email format")
    @NotBlank(message = "User email is required")
    private String email;

    @NotEmpty(message = "Please enter your name")
    private String name;

    @NotEmpty(message = "Password is required")
    @Size(min = 6)
    private String password;

}
