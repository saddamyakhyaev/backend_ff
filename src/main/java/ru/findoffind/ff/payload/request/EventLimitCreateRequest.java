package ru.findoffind.ff.payload.request;

import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Объект данных для создания границы событий от клиента
 */
@Data
public class EventLimitCreateRequest {


    private Long id;

    @NotEmpty(message = "Name cannot be empty")
    @Size(min = 3, max = 50)
    private String name;

    @NotNull(message = "TimeFrom cannot be empty")
    private LocalTime timeFrom;

    @NotNull(message = "TimeTo cannot be empty")
    private LocalTime timeTo;
}
