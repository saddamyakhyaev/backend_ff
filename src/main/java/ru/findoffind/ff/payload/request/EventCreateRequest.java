package ru.findoffind.ff.payload.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Объект данных для создания события от клиента
 */
@Data
public class EventCreateRequest {

    private Long id;

    @NotEmpty(message = "Description cannot be empty")
    @Size(min = 5, max = 1000)
    private String description;

    private String information;

    @NotEmpty(message = "clipsId cannot be empty")
    @Size(min = 1, max = 20)
    private List<Long> clipsId;

    @NotNull(message = "EventSeasonId cannot be empty")
    private Long eventSeasonId;

    @NotEmpty(message = "Times cannot be empty")
    @Size(min = 1, max = 20)
    private List<EventTimeCreateRequest> times;




}
