package ru.findoffind.ff.payload.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Обьект данных для создания закрепа от админа
 */
@Data
public class DonatCreateRequest {

    @NotNull(message = "Clip Id cannot be empty")
    private Long clipId;

    @NotNull(message = "User Id cannot be empty")
    private Long userId;

    @NotNull(message = "Coins Id cannot be empty")
    private Integer coins;


}
