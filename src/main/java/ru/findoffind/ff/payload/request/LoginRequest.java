package ru.findoffind.ff.payload.request;

import lombok.Data;
import ru.findoffind.ff.payload.request.tool.ReCaptchaRequest;

import javax.validation.constraints.NotEmpty;

/**
 * Обьект данных для входа от клиента
 */
@Data
public class LoginRequest extends ReCaptchaRequest {

    @NotEmpty(message = "Username cannot be empty")
    private String username;

    @NotEmpty(message = "Password cannot be empty")
    private String password;

    /**
     * Ключ от бота для авторизации без каптчи
     */
    private String botKey;

}
