package ru.findoffind.ff.payload.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Объект данных для создания рекламы от клиента
 */
@Data
public class AdsCreateRequest {
    @NotEmpty(message = "Name cannot be empty")
    @Size(min = 3, max = 100)
    private String name;

    @NotEmpty(message = "Description cannot be empty")
    @Size(min = 10, max = 300)
    private String description;

    private String link;

    private String text;

    @NotEmpty(message = "imageUrlPc cannot be empty")
    private String imageUrlPc;

    @NotEmpty(message = "imageUrlMobil cannot be empty")
    private String imageUrlMobil;

    @NotNull(message = "DateFrom cannot be empty")
    private LocalDateTime dateFrom;

    @NotNull(message = "DateTo cannot be empty")
    private LocalDateTime dateTo;

}
