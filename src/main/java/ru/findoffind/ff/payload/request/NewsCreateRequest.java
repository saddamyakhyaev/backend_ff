package ru.findoffind.ff.payload.request;

import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;


/**
 * Объект данных для создания новости от клиента
 */
@Data
public class NewsCreateRequest {

    private Long id;

    @NotEmpty(message = "Name cannot be empty")
    @Size(min = 1, max = 100)
    private String name;

    private String nameUnique;

    @NotEmpty(message = "Description cannot be empty")
    @Size(min = 1, max = 300)
    private String description;

    @NotEmpty(message = "Description cannot be empty")
    @Size(min = 2)
    private String text;

    @NotNull(message = "DateFrom cannot be empty")
    private LocalDate dateFrom;

    @NotNull(message = "DateTo cannot be empty")
    private LocalDate dateTo;

    private List<Long> marksId;

    private Boolean isNotification;
}
