package ru.findoffind.ff.payload.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Обьект данных для создания закрепа от клиента
 */
@Data
public class ClipCreateRequest {

    private Long id;

    @NotEmpty(message = "Name cannot be empty")
    @Size(min = 3, max = 100)
    private String name;

    private String information;

    private String description;

    @NotNull(message = "ClipTypeId cannot be empty")
    private Long clipTypeId;

    private Long markId;
}
