package ru.findoffind.ff.payload.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * Объект данных для создания сезона событий от клиента
 */
@Data
public class EventSeasonCreateRequest {

    private Long id;

    @NotEmpty(message = "Name cannot be empty")
    @Size(min = 3, max = 50)
    private String name;

    @NotEmpty(message = "Description cannot be empty")
    @Size(min = 3, max = 200)
    private String description;

    @NotNull(message = "DateFrom cannot be empty")
    private LocalDate dateFrom;

    @NotNull(message = "DateTo cannot be empty")
    private LocalDate dateTo;
}
