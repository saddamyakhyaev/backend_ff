package ru.findoffind.ff.payload.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

/**
 * Объект данных для создания времени события от клиента
 */
@Data
public class EventTimeCreateRequest {

    @NotNull(message = "DateFrom cannot be empty")
    private LocalTime timeFrom;

    @NotNull(message = "DateTo cannot be empty")
    private LocalTime timeTo;

    @NotEmpty(message = "Dates cannot be empty")
    @Size(min = 1, max = 200)
    private Set<LocalDate> dates;
}
