package ru.findoffind.ff.dto;

import lombok.Data;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.UserBox;
import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.enums.ERole;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

/**
 * Представление пользователя для клиента {@link User}
 */
@Data
public class UserDto {

    /**
     * {@link User#getId()}
     */
    private Long id;

    /**
     * {@link User#getName()}
     */
    private String name;

    /**
     * {@link User#getEmail()}
     */
    private String email;

    /**
     * {@link User#getState()}
     */
    private Integer state;


    /**
     * {@link User#getProtectedId()}
     */
    private Long protectedId;

    /**
     * {@link User#getRoles()}
     */
    private Integer role;

    private UserBox userBox;


}
