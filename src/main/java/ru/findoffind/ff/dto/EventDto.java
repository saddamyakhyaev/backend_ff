package ru.findoffind.ff.dto;

import lombok.Data;
import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.event.Event;
import ru.findoffind.ff.entity.event.EventSeason;
import ru.findoffind.ff.entity.event.EventTime;
import ru.findoffind.ff.facade.ClipFacade;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Представление события для клиента {@link Event}
 */
@Data
public class EventDto {

    /**
     * {@link Event#getId()}
     */
    private Long id;

    /**
     * {@link Event#getDescription()}
     */
    private String description;

    /**
     * {@link Event#getInformation()}
     */
    private String information;

    /**
     * {@link Event#getClips()}
     */
    private List<ClipDto> clips = new ArrayList<>();

    /**
     * {@link Event#getTimes()}
     */
    private Set<EventTime> dates = new HashSet<>();

    /**
     * {@link Event#getSeason()}
     */
    private EventSeason season;

    /**
     * {@link Event#getCreateUserId()}
     */
    private Long createUserId;

    /**
     * {@link Event#getCreatedDate()}
     */
    private LocalDateTime createdDate;

}
