package ru.findoffind.ff.dto;

import lombok.Data;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.event.Event;

import javax.persistence.*;

/**
 * Представление закрепа для клиента {@link Clip}
 */
@Data
public class ClipDto {

    /**
     * {@link Clip#getId()}
     */
    private Long id;

    /**
     * {@link Clip#getTypeClipId()}
     */
    private Long typeClipId;

    /**
     * {@link Clip#getName()}
     */
    private String name;

    /**
     * {@link Clip#getDescription()}
     */
    private String description;

    /**
     * {@link Clip#getInformation()}
     */
    private String information;

    /**
     * {@link Clip#getState()}
     */
    private Integer state;

    /**
     * {@link Clip#getMarkId()}
     */
    private Long markId;

}
