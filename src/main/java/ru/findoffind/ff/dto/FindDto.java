package ru.findoffind.ff.dto;

import lombok.Data;

import java.util.List;

@Data
public class FindDto {

    private List<ClipDto> clips;
    private List<UserDto> users;
}
