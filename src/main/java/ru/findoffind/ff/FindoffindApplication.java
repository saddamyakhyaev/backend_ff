package ru.findoffind.ff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import ru.findoffind.ff.security.SecurityConstants;

import java.util.Date;

@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
public class FindoffindApplication {

	public static void main(String[] args) {
		SpringApplication.run(FindoffindApplication.class, args);
	}

}
