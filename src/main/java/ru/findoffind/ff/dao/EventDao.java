package ru.findoffind.ff.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.clip.ClipType;
import ru.findoffind.ff.entity.enums.EState;
import ru.findoffind.ff.entity.event.Event;
import ru.findoffind.ff.entity.event.EventLimit;
import ru.findoffind.ff.entity.event.EventSeason;
import ru.findoffind.ff.entity.event.EventTime;

import java.time.LocalDate;
import java.util.List;

@Repository
public class EventDao {

    @Autowired
    private SessionFactory sessionFactory;


    /**
     * Сохранить сезон событий
     *
     * @param eventSeason сам объект для сохранения
     * @return результат
     */
    public EventSeason saveEventSeason(EventSeason eventSeason) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(eventSeason);
        return eventSeason;
    }

    /**
     * Возвращаем Сезон по ID
     *
     * @param id сезона
     * @return
     */
    public EventSeason getEventSeasonById(Long id) {

        Session session = sessionFactory.getCurrentSession();
        EventSeason eventSeason = session.get(EventSeason.class, id);

        return eventSeason;
    }

    /**
     * Получить список всех сезонов событий
     *
     * @return список сезонов событий
     */
    public List<EventSeason> getAllEventSeasons() {

        Session session = sessionFactory.getCurrentSession();
        Query<EventSeason> query = session.createQuery("from EventSeason",
                EventSeason.class);
        List<EventSeason> eventSeasons = query.getResultList();

        return eventSeasons;
    }

    /**
     * Удаляем сезон
     */
    public boolean deleteEventSeason(EventSeason eventSeason) {

        Session session = sessionFactory.getCurrentSession();
        session.delete(eventSeason);
        return true;
    }

    /**
     * Сохранить границы события
     *
     * @param eventLimit сам объект для сохранения
     * @return результат
     */
    public boolean saveEventLimit(EventLimit eventLimit) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(eventLimit);
        return true;
    }


    /**
     * Возвращаем Лимит по ID
     *
     * @param id лимита
     * @return
     */
    public EventLimit getEventLimitById(Long id) {

        Session session = sessionFactory.getCurrentSession();
        EventLimit eventLimit = session.get(EventLimit.class, id);

        return eventLimit;
    }

    /**
     * Получить список всех границ событий
     *
     * @return список сезонов событий
     */
    public List<EventLimit> getAllEventLimits() {

        Session session = sessionFactory.getCurrentSession();
        Query<EventLimit> query = session.createQuery("from EventLimit",
                EventLimit.class);
        List<EventLimit> eventLimits = query.getResultList();

        return eventLimits;
    }

    /**
     * Удаляем лимит
     */
    public boolean deleteEventLimit(EventLimit eventLimit) {

        Session session = sessionFactory.getCurrentSession();
        session.delete(eventLimit);

        return true;
    }

    /**
     * Сохранить время события
     *
     * @param eventTime сам объект для сохранения
     * @return результат
     */
    public boolean saveEventTime(EventTime eventTime) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(eventTime);
        return true;
    }

    /**
     * Сохранить событие
     *
     * @param event сам объект для сохранения
     * @return результат
     */
    public boolean saveEvent(Event event) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(event);
        return true;
    }

    /**
     * Получить список всех событий
     */
    public List<Event> getAllEventByClip(Long clipId) {

        Session session = sessionFactory.getCurrentSession();
        Query<Event> query = session.createQuery("select DISTINCT event from Event event" +
                        " inner join event.clips clip" +
                        " where clip.id = :clipId" +
                        " and event.state = :state" +
                        " and event.season.state = :stateSeason" +
                        " and event.season.dateTo >= :dateTo",
                Event.class);

        query.setParameter("clipId", clipId);
        query.setParameter("state", EState.STATE_NORMAL.ordinal());
        query.setParameter("stateSeason", EState.STATE_NORMAL.ordinal());
        query.setParameter("dateTo", LocalDate.now());

        List<Event> events = query.getResultList();

        return events;
    }

    /**
     * Получаем все события по закрепу определенного сезона
     */
    public List<Event> getAllEventByClipOfSeason(Long clipId) {

        Session session = sessionFactory.getCurrentSession();
        Query<Event> query = session.createQuery("select DISTINCT event from Event event" +
                        " inner join event.clips clip" +
                        " where clip.id = :clipId" +
                        " and event.state = :state" +
                        " and event.season.state = :stateSeason" +
                        " and event.season.dateTo >= :dateTo",
                Event.class);

        query.setParameter("clipId", clipId);
        query.setParameter("state", EState.STATE_NORMAL.ordinal());
        query.setParameter("stateSeason", EState.STATE_NORMAL.ordinal());
        query.setParameter("dateTo", LocalDate.now());

        List<Event> events = query.getResultList();

        return events;
    }


    /**
     * Получить список событий по Сезону
     */
    public List<Event> getAllEventBySeason(EventSeason season) {

        Session session = sessionFactory.getCurrentSession();

        Query<Event> query = session.createQuery("from Event event" +
                        " WHERE event.season.id = :seasonId",
                Event.class).setMaxResults(200);

        query.setParameter("seasonId", season.getId());

        List<Event> events = query.getResultList();

        return events;
    }

    /**
     * Получить список всех событий для пользователя
     * @param userId id пользователя
     * @return список событий
     */
    public List<Event> getAllEventByUser(Long userId) {

        Session session = sessionFactory.getCurrentSession();
        Query<Event> query = session.createQuery("from Event event" +
                        " where event.createUserId = :userId" +
                        " and event.state != :state",
                Event.class);

        query.setParameter("userId", userId);
        query.setParameter("state", EState.STATE_DELETE.ordinal());

        List<Event> events = query.getResultList();

        return events;
    }

    /**
     * Возвращаем Событие по ID
     * @param id события
     * @return
     */
    public Event getEventById(Long id) {

        Session session = sessionFactory.getCurrentSession();
        Event event = session.get(Event.class, id);
        return event;
    }

    /**
     * Удаляем событие
     */
    public boolean deleteEvent(Event event) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(event);

        return true;
    }

}
