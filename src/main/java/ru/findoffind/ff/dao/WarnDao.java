package ru.findoffind.ff.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.findoffind.ff.entity.UserBox;
import ru.findoffind.ff.entity.enums.EState;
import ru.findoffind.ff.entity.event.Event;
import ru.findoffind.ff.entity.warn.*;

import java.util.List;


@Repository
public class WarnDao {

    @Autowired
    private SessionFactory sessionFactory;


    /**
     * Получить список последних 200 предупреждений
     */
    public List<Warn> getLastWarns(boolean max) {

        Session session = sessionFactory.getCurrentSession();
        Query<Warn> query = session.createQuery("from Warn warn",
                Warn.class);

        if(max) query.setMaxResults(1000);
        List<Warn> warns = query.getResultList();

        return warns;
    }

    /**
     * Получить список последних 200 предупреждений
     */
    public List<Warn> getLastWarnsByUser(Long userId) {

        Session session = sessionFactory.getCurrentSession();
        Query<Warn> query = session.createQuery("from Warn warn" +
                " where warn.userId = :userId",
                Warn.class).setMaxResults(1000);

        query.setParameter("userId", userId);
        List<Warn> warns = query.getResultList();

        return warns;
    }

    public boolean saveWarn(Warn warn) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(warn);
        return true;
    }

    /**
     * Удаляем предупреждение
     */
    public boolean deleteWarn(Warn warn) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(warn);

        return true;
    }
}
