package ru.findoffind.ff.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.findoffind.ff.entity.ads.Ads;
import ru.findoffind.ff.entity.ads.AdsBody;
import ru.findoffind.ff.entity.news.News;
import ru.findoffind.ff.entity.news.NewsBody;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class AdsDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Сохранить рекламу
     * @param ads сама реклама
     * @return результат
     */
    public boolean saveNews(Ads ads) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(ads);
        return true;
    }

    /**
     * Сохранить тело рекламы
     * @param adsBody само тело рекламы
     * @return результат
     */
    public boolean saveAdsBody(AdsBody adsBody) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(adsBody);
        return true;
    }

    /**
     * Возвращаем рекламу по id
     * @param id рекламы
     * @return искомая реклама
     */
    public Ads getAds(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Ads ads = session.get(Ads.class, id);
        return ads;
    }

    /**
     * Возвращаем тело рекламы по id
     * @param id рекламы
     * @return искомое тело
     */
    public AdsBody getAdsBody(Long id) {
        Session session = sessionFactory.getCurrentSession();
        AdsBody adsBody = session.get(AdsBody.class, id);
        return adsBody;
    }

    /**
     * Получить список всех активных реклам
     * @return список реклам
     */
    public List<Ads> getAllActiveAds() {

        Session session = sessionFactory.getCurrentSession();
        Query<Ads> query = session.createQuery("from Ads ads" +
                                " where ads.isActive = true" +
                                " and ads.dateFrom <= :dateNow" +
                                " and ads.dateTo >= :dateNow",
                        Ads.class)
                .setParameter("dateNow", LocalDateTime.now());

        List<Ads> ads = query.getResultList();

        return ads;
    }
}
