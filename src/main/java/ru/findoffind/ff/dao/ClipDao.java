package ru.findoffind.ff.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.clip.ClipType;

import java.util.List;

@Repository
public class ClipDao {

    @Autowired
    private SessionFactory sessionFactory;


    /**
     * Получить тип закрепа
     * @param id закрепа
     * @return
     */
    public ClipType getTypeClip(Long id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(ClipType.class, id);
    }

    /**
     * Получить список всех типов закрепа
     * @return список типов закрепа
     */
    public List<ClipType> getClipTypes(){

        Session session = sessionFactory.getCurrentSession();
        Query<ClipType> query = session.createQuery("from ClipType",
                ClipType.class);
        List<ClipType> clipTypes = query.getResultList();

        return clipTypes;
    }


    /**
     * Возвращаем закреп по id
     * @param id закрепа
     * @return искомый закреп
     */
    public Clip getClip(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Clip clip = session.get(Clip.class, id);
        return clip;
    }

    /**
     * Возвращаем все закрепы созданные определенным пользователем
     */
    public List<Clip> getAllClips() {
        Session session = sessionFactory.getCurrentSession();
        Query<Clip> query = session.createQuery("from Clip clip",
                        Clip.class);

        return query.getResultList();
    }

    /**
     * Возвращаем все закрепы созданные определенным пользователем
     * @param userId пользователя
     * @return искомые закрепы
     */
    public List<Clip> getClipsCreatingByUser(Long userId) {
        Session session = sessionFactory.getCurrentSession();
        Query<Clip> query = session.createQuery("from Clip clip" +
                                " where clip.createUserId = :userId",
                        Clip.class)
                .setParameter("userId", userId);

        return query.getResultList();
    }

    /**
     * Возвращает закреп по названию
     * @param name навзание закрепа
     * @return
     */
    public Clip getClipOfName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Clip> query = session.createQuery("from Clip" +
                                " where name = :name",
                        Clip.class)
                .setParameter("name", name);

        return query.getResultList().size() > 0 ? query.getSingleResult(): null;
    }

    /**
     * Сохранить закреп
     * @param clip сам закреп
     * @return результат
     */
    public boolean saveClip(Clip clip) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(clip);
        return true;
    }

    /**
     * Метод для поиска в БД закрепов по строке
     * @param str сама строка
     * @return список искомых закрепов
     */
    public List<Clip> getClipsToFindToName(String str) {

        Session session = sessionFactory.getCurrentSession();
        Query<Clip> query = session.createQuery("from Clip clip"
                                + " where clip.name like :str",
                        Clip.class).setMaxResults(7)
                .setParameter("str", "%" + str + "%");

        List<Clip> clips = query.getResultList();

        return clips;
    }
}
