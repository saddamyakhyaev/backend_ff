package ru.findoffind.ff.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.event.EventSeason;
import ru.findoffind.ff.entity.news.News;
import ru.findoffind.ff.entity.news.NewsBody;

import java.time.LocalDate;
import java.util.List;

@Repository
public class NewsDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Сохранить новость
     * @param news сама новость
     * @return результат
     */
    public boolean saveNews(News news) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(news);
        return true;
    }

    /**
     * Сохранить тело новости
     * @param newsBody само тело новости
     * @return результат
     */
    public boolean saveNewsBody(NewsBody newsBody) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(newsBody);
        return true;
    }

    /**
     * Возвращаем новость по id
     * @param id новости
     * @return искомая новость
     */
    public News getNews(Long id) {
        Session session = sessionFactory.getCurrentSession();
        News news = session.get(News.class, id);
        return news;
    }

    /**
     * Возвращаем тело новости по id
     * @param id новости
     * @return искомое тело
     */
    public NewsBody getNewsBody(Long id) {
        Session session = sessionFactory.getCurrentSession();
        NewsBody newsBody = session.get(NewsBody.class, id);
        return newsBody;
    }

    /**
     * Получить список всех активных новостей
     * @return список новостей
     */
    public List<News> getAllActiveNews() {

        Session session = sessionFactory.getCurrentSession();
        Query<News> query = session.createQuery("from News news" +
                        " where news.isActive = true" +
                        " and news.dateFrom <= :dateNow" +
                        " and news.dateTo >= :dateNow",
                News.class)
                .setParameter("dateNow", LocalDate.now());

        List<News> news = query.getResultList();

        return news;
    }
}
