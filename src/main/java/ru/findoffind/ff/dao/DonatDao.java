package ru.findoffind.ff.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.findoffind.ff.entity.clip.Clip;
import ru.findoffind.ff.entity.clip.ClipType;
import ru.findoffind.ff.entity.donation.Donat;

import java.util.List;

@Repository
public class DonatDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Получить донат по ID
     */
    public Donat getDonatById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Donat.class, id);
    }

    /**
     * Получить список всех донатов
     */
    public List<Donat> getAllDonats(){

        Session session = sessionFactory.getCurrentSession();
        Query<Donat> query = session.createQuery("from Donat",
                Donat.class);
        List<Donat> donats = query.getResultList();

        return donats;
    }

    /**
     * Сохранить донат
     */
    public boolean saveDonat(Donat donat) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(donat);
        return true;
    }
}
