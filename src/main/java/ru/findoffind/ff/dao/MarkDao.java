package ru.findoffind.ff.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.findoffind.ff.entity.enums.EState;
import ru.findoffind.ff.entity.event.Event;
import ru.findoffind.ff.entity.event.EventLimit;
import ru.findoffind.ff.entity.event.EventSeason;
import ru.findoffind.ff.entity.event.EventTime;
import ru.findoffind.ff.entity.mark.Mark;

import java.time.LocalDate;
import java.util.List;

@Repository
public class MarkDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Сохранить сезон марку
     */
    public boolean saveMark(Mark mark) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(mark);
        return true;
    }

    /**
     * Возвращаем Марку по ID
     */
    public Mark getMarkById(Long id) {

        Session session = sessionFactory.getCurrentSession();
        Mark mark = session.get(Mark.class, id);

        return mark;
    }


    /**
     * Получить список всех марок
     */
    public List<Mark> getAllMark() {
        Session session = sessionFactory.getCurrentSession();
        Query<Mark> query = session.createQuery("from Mark",
                Mark.class);
        List<Mark> marks = query.getResultList();

        return marks;
    }
}
