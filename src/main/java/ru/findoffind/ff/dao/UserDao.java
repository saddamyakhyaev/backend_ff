package ru.findoffind.ff.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.findoffind.ff.entity.User;
import ru.findoffind.ff.entity.UserBox;
import ru.findoffind.ff.entity.enums.ERole;
import ru.findoffind.ff.entity.enums.EState;
import ru.findoffind.ff.entity.event.Event;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Repository
public class UserDao {

    @Autowired
    private SessionFactory sessionFactory;


    public List<User> getAllUsers() {

        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("from User",
                User.class);
        List<User> users = query.getResultList();

        return users;
    }

//    public List<User> getUsersToFindToName(String strName, List<ERole> roles) {
//
//        Session session = sessionFactory.getCurrentSession();
//        Query<User> query = session.createQuery("from User user"
//                + " join fetch user.roles role"
//                + " where role IN (:roleLevel)",
//                User.class)
//                .setParameter("roleLevel", roles);
//
//        List<User> users = query.getResultList();
//
//        return users;
//    }

    /**
     * Метод для поиска в БД пользователей по строке
     * @param str сама строка
     * @return список искомых пользователей
     */
    public List<User> getUsersToFindToName(String str) {

        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("from User user"
                                + " where user.name like :str",
                        User.class).setMaxResults(10)
                .setParameter("str", "%" + str + "%")
                .setMaxResults(6);

        List<User> users = query.getResultList();

        return users;
    }


    /**
     * Метод для поиска в БД пользователей по ID
     */
    public List<User> getUsersToFindById(Long id) {

        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("from User user"
                                + " where user.id = :id",
                        User.class).setMaxResults(10)
                .setParameter("id", id)
                .setMaxResults(6);

        List<User> users = query.getResultList();

        return users;
    }

    /**
     * Получить пользователя по ID
     * @param id пользователя
     * @return пользователь
     */
    public User getUser(Long id) {

        Session session = sessionFactory.getCurrentSession();
        User user = session.get(User.class, id);

        return user;
    }

    /**
     * Получить личные данные пользователя
     * @param id пользователя
     * @return личные данные
     */
    public UserBox getUserBox(Long id) {

        Session session = sessionFactory.getCurrentSession();
        UserBox userBox = session.get(UserBox.class, id);

        return userBox;
    }

    /**
     * Получить пользователя по почте
     * @param email почта
     * @return пользователь
     */
    public User getUserOfEmail(String email) {
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("from User" +
                " where email = :email",
                User.class)
                .setParameter("email", email);

        return query.getResultList().size() > 0 ? query.getSingleResult(): null;
    }

    /**
     * Возвращаем список всех пользователей модераторов
     */
    public List<User> getUsersIsModerator() {

        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("select DISTINCT user from User user" +
                        " inner join user.roles role " +
                        " where role IN (" + ERole.ROLE_MODERATOR_2.ordinal() + "," + ERole.ROLE_MODERATOR_3.ordinal() + ")",
                User.class);

        List<User> users = query.getResultList();

        return users;
    }




    public boolean saveUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(user);
        return true;
    }


    public boolean saveUserBox(UserBox userBox) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(userBox);
        return true;
    }




}
